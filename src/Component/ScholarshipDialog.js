import React from "react";
import { GrFormClose } from "react-icons/gr";
import { AiOutlineClose } from "react-icons/ai";

export const ScholarshipDialog = () => {
  const iconStyle = {
    height: "30px",
    width: "30px",
    color: "#fff",
    right: "25px",
    position: "absolute",
    top: "10px",
    cursor: "pointer",
  };
  return (
    <>
      {/* <button
        type="button"
        class="btn btn-primary"
        data-toggle="modal"
        data-target="#scholarshipModal"
      >
        Apply Scholarship
      </button> */}

      <div
        class="modal fade scholarship-style"
        id="scholarshipModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="scholarshipModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog " role="document">
          <div class="modal-content text-white">
            <div className="mx-5">
              <div class="modal-header">
                <button
                  type="button"
                  class="btn scholar-btn"
                  data-dismiss="modal"
                >
                  <AiOutlineClose style={iconStyle} />
                </button>
              </div>
              <div>
                <h1 className="scholar-heading">Apply for Scholarship</h1>
                <p className="scholar-subheading">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy
                </p>
              </div>
              <div>
                <form>
                  <div className="mb-3">
                    <label>Name</label>
                    <br />
                    <input type="name" className="no-outline " />
                  </div>
                  <div className="mb-3">
                    <label>Email</label>
                    <br />
                    <input type="name" className="no-outline " />
                  </div>
                  <div className="mb-3">
                    <label>Contact Number</label>
                    <br />
                    <input type="name" className="no-outline " />
                  </div>
                  <div className="mb-3">
                    <select className="form-select no-outline ps-0 pr-0">
                      <option selected>Course</option>
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                    </select>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="mb-3 scholar-input">
                        {/* <label>Name</label>
                        <br />
                        <input type="name" className="no-outline  " /> */}

                        <select
                          className="form-select no-outline ps-0 pr-0"
                          aria-label="Default select example"
                        >
                          <option selected>Course Type</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="mb-3 scholar-input">
                        <select
                          className="form-select no-outline ps-0 pr-0"
                          aria-label="Default select example"
                        >
                          <option selected>Catagory</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="mb-3">
                    <label>Desingnation</label>
                    <br />
                    <input type="name" className="no-outline " />
                  </div>
                </form>
                <div className="mb-5">
                  <button type="submit" className="btn apply-btn ">
                    Apply Now
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
