import React, { useState } from "react";

export const OpenableCard = (props) => {
  const [show, setShow] = useState(true);
  console.log("Props", props);
  return (
    <div>
      {" "}
      <div className="container ">
        {show ? (
          <div
            className="card subcard mt-4  "
            style={{ color: " #0031F2", backgroundColor: "#E9E9E9" }}
          >
            {/* {props.item.id}.<span className=" ms-3">{props.item.cardHead}</span> */}

            <span onClick={() => setShow(false)} className="ms-5 mt-4 mb-4">
              {props.item.cardHead}
            </span>
          </div>
        ) : (
          <div className="card subcard h-100 mt-4">
            <span
              onClick={() => setShow(true)}
              className="ms-5 mt-4 "
              style={{ color: "#0031F2" }}
            >
              {props.item.id}.
              <span className=" ms-3 ">
                <b>{props.item.cardHead}</b>
              </span>
            </span>
            <p className="ps-5 ms-4 mt-3" style={{ color: "black" }}>
              {props.item.cardContent}
            </p>
          </div>
        )}
      </div>
    </div>
  );
};
