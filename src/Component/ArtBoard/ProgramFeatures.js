import React from "react";
import desImage from "../../Asserts/Images/desimage.png";
import placementAssistance from "../../Asserts/Images/placement_assistance_817.png";
import industryNetworking from "../../Asserts/Images/industry_networking_817.png";

function ProgramFeatures() {
  return (
    <div className="container prf">
      <div className="program-head">
        <h1 className="program-head">Program Features</h1>
      </div>
      <div className="row">
        <div className="col-sm-4">
          <div className="text-center">
            <div
              className="card program wow animate__ animate__rollIn animate__slower   animated"
              //   style="visibility: visible; animation-name: rollIn;"
              style={{
                visibility: "visible",
                animationName: "rollIn",
                backgroundColor: "#1f6f8b",
                border: "none",
                boxShadow: "none",
              }}
            >
              <img src={desImage} className="prg-img" />
              <div className="card-body">
                <img src={placementAssistance} />
                <h3
                  className="card-subtitle mb-2 program-card-text"
                  style={{ marginTop: "25px", color: "white" }}
                >
                  Placement Assistance
                </h3>
                <p
                  className="card-text program-card-text2"
                  style={{ color: "white" }}
                >
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type{" "}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="col-sm-4">
          <div className="text-center">
            <div
              className="card program2 wow animate__ animate__jackInTheBox animate__slower   animated"
              //   style="visibility: visible; animation-name: jackInTheBox;"
              style={{ visibility: "visible", animationName: "jackInTheBox" }}
            >
              <div className="card-body">
                <img src={industryNetworking} />
                <h3
                  className="card-subtitle mb-2 program-card-text3"
                  style={{ marginTop: "25px" }}
                >
                  Placement Assistance
                </h3>
                <p className="card-text program-card-text4">
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type{" "}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="col-sm-4">
          <div className="text-center">
            <div
              className="card program wow animate__ animate__rollIn animate__slower   animated"
              //   style="visibility: visible; animation-name: rollIn;"
              style={{
                visibility: "visible",
                animationName: "rollIn",
                backgroundColor: "#1f6f8b",
                border: "none",
                boxShadow: "none",
              }}
            >
              <img src={desImage} className="prg-img" />
              <div className="card-body">
                <img src={placementAssistance} />
                <h3
                  className="card-subtitle mb-2 program-card-text"
                  style={{ marginTop: "25px", color: "white" }}
                >
                  Placement Assistance
                </h3>
                <p
                  className="card-text program-card-text2"
                  style={{ color: "white" }}
                >
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type{" "}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProgramFeatures;
