import React from "react";
import "animate.css";

function ApplyScholarshipSection() {
  return (
    <div className="scholarship-section">
      <div className="scholarship-header animate__animated animate__lightSpeedInLeft">
        Get a chance to win a scholarship
      </div>
      <div className="scholarship-btn animate__animated animate__lightSpeedInRight">
        Apply Scholarship
      </div>
    </div>
  );
}

export default ApplyScholarshipSection;
