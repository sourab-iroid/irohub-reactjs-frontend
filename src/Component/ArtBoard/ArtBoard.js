import React from "react";
import FirstSection from "./FirstSection";
import Reviewer from "../../Asserts/Images/Reviewer.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Tabs, Tab, TabPanel, TabList } from "react-web-tabs";
import "react-tabs/style/react-tabs.css";
import { Table, Thead, Tbody, Tr, Th, Td } from "react-super-responsive-table";
import "react-super-responsive-table/dist/SuperResponsiveTableStyle.css";
import CategorySection from "./CategorySection";
import SyllabusSection from "./SyllabusSection";
import ApplyScholarshipSection from "./ApplyScholarshipSection";
import LectureCaed from "./LectureCaed";
import {
  AiFillStar,
  AiOutlineClockCircle,
  AiOutlineStar,
} from "react-icons/ai";
import { TiTickOutline } from "react-icons/ti";
import ProgramFeatures from "./ProgramFeatures";
function ArtBoard() {
  let [courseSectionClicked, setCourseSectionClicked] = React.useState("");
  React.useEffect(() => {
    let courseSection = localStorage
      .getItem("courseSectionClicked")
      .split("Development")[0];
    setCourseSectionClicked(courseSection);
  }, []);
  const courseTickedItems = [
    "Shareable Specialization and Course Certificates",
    "Course Videos & Readings",
    "Access to our online community",
    "Graded Assignments with Peer Feedback",
  ];

  return (
    <div>
      <div className="artboard-container">
        <FirstSection />
        <CategorySection />
        <div className="time-table-container ">
          <div className="header-text animate__animated animate__backInDown animate__slow">
            Live Online Classes{" "}
          </div>
          <div className="table-x animate__animated animate__zoomIn animate__slow container table-responsive p-3">
            <Table>
              <Thead>
                <Tr>
                  <Th>Starting Date</Th>
                  <Th>Weeks</Th>
                  <Th>Time / Available Seats</Th>
                </Tr>
              </Thead>
              <Tbody>
                <Tr>
                  <Td>Jan 3th</Td>
                  <Td>Mon - Fri ( 8 Weeks)</Td>
                  <Td>
                    <div>
                      {/* <div className="sold-out">Sold Out</div> */}
                      <div className="available-seats">10 seat available</div>

                      <div className="time">
                        Timings - 07:00 AM to 10:00 AM (IST)
                      </div>

                      <div className="reschedule">Reshedule</div>
                    </div>
                  </Td>
                </Tr>
                <Tr>
                  <Td>Jan 3th</Td>
                  <Td>Mon - Fri ( 8 Weeks)</Td>
                  <Td>
                    <div>
                      {/* <div className="sold-out">Sold Out</div> */}
                      <div className="available-seats">10 seat available</div>
                      <div className="time">
                        Timings - 07:00 AM to 10:00 AM (IST)
                      </div>
                      <div className="reschedule">Reshedule</div>
                    </div>
                  </Td>
                </Tr>
              </Tbody>
            </Table>
          </div>
        </div>
        <SyllabusSection />
        <ApplyScholarshipSection />
        {/*  */}
        <ProgramFeatures />

        <div className="lectures">
          <div className="lecture-header-text">Learn with the best</div>
          <div className="lectures-container">
            {[1, 2, 3].map((item, index) => {
              return <LectureCaed />;
            })}
          </div>
        </div>
        <div className="student-top-reviews">
          <div className="student-review-text animate__animated animate__fadeInBottomLeft animate__slow">
            Top Student Reviews
          </div>
          <div className="student-review-contents animate__animated animate__fadeIn animate__slow">
            {[1, 2, 3, 4, 5, 6].map((item, index) => {
              return (
                <div className="review-card">
                  <div className="review-card-text">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua. At vero eos et accusam et
                    justo duo dolores et ea rebum. Stet clita kasd gubergren, no
                    sea takimata sanctus est Lorem ipsum dolor sit amet.
                  </div>
                  <div className="review-owner-detail">
                    <div className="review-owner-user-image">
                      <img src={Reviewer} />
                    </div>
                    <div className="review-owner-user-name-rating">
                      {/* */}
                      <div className="review-owner-user-name">Anna Blown</div>
                      {/* <span class="fa fa-star checked"></span>
                      <span class="fa fa-star checked"></span>
                      <span class="fa fa-star checked"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span> */}
                      {/* <div className="review-owner-user-rating"></div> */}
                      {[1, 2, 3, 4, 5].map((v, i) => {
                        while (i < 4) {
                          return <AiOutlineStar style={{ color: "yellow" }} />;
                        }
                        return <AiOutlineStar />;
                      })}
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div class="container-fluid crs-des">
          <div
            class="last-section wow animate__ animate__fadeIn animate__slow   animated"
            style={{ visibility: "visible" }}
          >
            <div class="container">
              <div class="row">
                <div class="col-md-7 animate__animated animate__fadeInLeftBig animate__slow">
                  <div
                    class="last wow animate__ animate__fadeInLeftBig animate__slow"
                    style={{ visibility: "visible" }}
                  >
                    <h1 class="last-head-crs-des">Start Learning Today</h1>
                    <div class="d-flex justify-content-start">
                      <TiTickOutline
                        style={{
                          color: "white",
                          position: "relative",
                          top: "-5px",
                          left: "10px",
                          fontSize: "xxx-large",
                        }}
                      />
                      {/* <fa-icon class="ng-fa-icon check-icons">
                        <svg
                          role="img"
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fas"
                          data-icon="check"
                          class="svg-inline--fa fa-check fa-w-16"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                        >
                          <path
                            fill="currentColor"
                            d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"
                          ></path>
                        </svg>
                      </fa-icon> */}
                      <p class="start-para">
                        Shareable Specialization and Course Certificates
                      </p>
                    </div>
                    <div class="d-flex justify-content-start mt-4">
                      <TiTickOutline
                        style={{
                          color: "white",
                          position: "relative",
                          top: "-5px",
                          left: "10px",
                          fontSize: "xxx-large",
                        }}
                      />
                      {/* <fa-icon class="ng-fa-icon check-icons">
                        <svg
                          role="img"
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fas"
                          data-icon="check"
                          class="svg-inline--fa fa-check fa-w-16"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                        >
                          <path
                            fill="currentColor"
                            d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"
                          ></path>
                        </svg>
                      </fa-icon> */}
                      <p class="start-para">Course Videos &amp; Readings</p>
                    </div>
                    <div class="d-flex justify-content-start mt-4">
                      <TiTickOutline
                        style={{
                          color: "white",
                          position: "relative",
                          top: "-5px",
                          left: "10px",
                          fontSize: "xxx-large",
                        }}
                      />
                      {/* <fa-icon class="ng-fa-icon check-icons">
                        <svg
                          role="img"
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fas"
                          data-icon="check"
                          class="svg-inline--fa fa-check fa-w-16"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                        >
                          <path
                            fill="currentColor"
                            d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"
                          ></path>
                        </svg>
                      </fa-icon> */}
                      <p class="start-para">
                        Graded Assignments with Peer Feedback
                      </p>
                    </div>
                    <div class="d-flex justify-content-start mt-4">
                      <TiTickOutline
                        style={{
                          color: "white",
                          position: "relative",
                          top: "-5px",
                          left: "10px",
                          fontSize: "xxx-large",
                        }}
                      />
                      {/* <fa-icon class="ng-fa-icon check-icons">
                        <svg
                          role="img"
                          aria-hidden="true"
                          focusable="false"
                          data-prefix="fas"
                          data-icon="check"
                          class="svg-inline--fa fa-check fa-w-16"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 512 512"
                        >
                          <path
                            fill="currentColor"
                            d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"
                          ></path>
                        </svg>
                      </fa-icon> */}
                      <p class="start-para">
                        Graded Assignments with Peer Feedback
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-md-5 animate__animated animate__fadeInRightBig animate__slow">
                  <div
                    class="price-square wow animate__ animate__fadeInRightBig animate__slow"
                    // style="visibility: visible;"
                    style={{ visibility: "visible" }}
                  >
                    <div class="start-box">
                      <h2 class="start-head text-center">GET STARTED WITH</h2>
                    </div>
                    <div class="texts">
                      <h1 class="start-subhead">
                        {courseSectionClicked} Developer
                      </h1>
                      <p class="beg-para">Beginner </p>
                      <div class="d-flex">
                        <h1 class="price">₹97045</h1>
                        <h1 class="text-muted act-price"> ₹97045</h1>
                      </div>
                      <div class="d-flex justify-content-between">
                        {/* <mat-icon
                          role="img"
                          class="mat-icon notranslate schedule material-icons mat-icon-no-color"
                          aria-hidden="true"
                          data-mat-icon-type="font"
                        >
                          schedule
                        </mat-icon> */}
                        <AiOutlineClockCircle
                          className="icon"
                          style={{
                            width: "18%",
                            position: "relative",
                            top: "5px",
                          }}
                        />
                        <p class="schedule-para">
                          Start learning today! Switch to the monthly price
                          afterwards If more time is needed.{" "}
                        </p>
                      </div>
                      <div class="d-flex justify-content-between">
                        {/* <mat-icon
                          role="img"
                          class="mat-icon notranslate schedule material-icons mat-icon-no-color"
                          aria-hidden="true"
                          data-mat-icon-type="font"
                        >
                          schedule
                        </mat-icon> */}
                        <AiOutlineClockCircle
                          className="icon"
                          style={{
                            width: "18%",
                            position: "relative",
                            top: "5px",
                          }}
                        />
                        <p class="schedule-para">
                          Start learning today! Switch to the monthly price
                          afterwards If more time is needed.{" "}
                        </p>
                      </div>
                      <div class="d-flex justify-content-between">
                        {/* <mat-icon
                          role="img"
                          class="mat-icon notranslate schedule material-icons mat-icon-no-color"
                          aria-hidden="true"
                          data-mat-icon-type="font"
                        >
                          schedule
                        </mat-icon> */}
                        <AiOutlineClockCircle
                          className="icon"
                          style={{
                            width: "18%",
                            position: "relative",
                            top: "5px",
                          }}
                        />
                        <p class="schedule-para">
                          Start learning today! Switch to the monthly price
                          afterwards If more time is needed.{" "}
                        </p>
                      </div>
                      <div class="choose-button text-center">
                        <button
                          type="button"
                          style={{
                            width: "180px",
                            height: "50px",
                            backgroundColor: "#5575f2",
                            color: "#fff",
                          }}
                          className="btn choose-plan"
                        >
                          Choose Plan
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ArtBoard;
