import React from "react";
import { OpenableCard } from "./OpenableCard";
import girlWithLaptop from "../../Asserts/Images/cute-girl-beige-shirt-touching-glasses-holding-laptop-with-smile.png";
import { AiFillFilePdf } from "react-icons/ai";
import "animate.css";
function SyllabusSection() {
  let [courseSectionClicked, setCourseSectionClicked] = React.useState("");
  React.useEffect(() => {
    let courseSection = localStorage.getItem("courseSectionClicked");
    setCourseSectionClicked(courseSection);
  }, []);
  const courseCardItems = [
    "450+ Hours live classes",
    "100+ Case Studies",
    "30+ Assignments",
    "20+ Projects",
  ];
  const cardData = [
    {
      id: 1,
      cardHead: "Onboarding Course",
      cardContent:
        "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla",
    },
    {
      id: 2,
      cardHead: "Onboarding Course",
      cardContent:
        "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla",
    },
    {
      id: 3,
      cardHead: "Onboarding Course",
      cardContent:
        "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla",
    },
    {
      id: 4,
      cardHead: "Onboarding Course",
      cardContent:
        "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla",
    },
    {
      id: 5,
      cardHead: "Onboarding Course",
      cardContent:
        "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla",
    },
  ];
  return (
    <div className="container syllabus-container">
      <div className="row">
        <div className="col-lg-6">
          <div className="course-syllabus animate__animated animate__fadeIn">
            <div className="tag-syllabus"> SYLLABUS</div>
            <div className="course-left-detail">
              <div className="course-header"> {courseSectionClicked}</div>
              <p
                className="course-description"
                style={{ textAlign: "justify" }}
              >
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. Lorem
                Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. Lorem
                Ipsum is simply dummy text of the printing and typesetting
                industry.
              </p>
              <div className="download-syllabus-btn">
                <AiFillFilePdf
                  style={{ color: "red", position: "relative", left: "-2px" }}
                />
                Download Syllabus
              </div>
              <div style={{ marginLeft: "-25px" }}>
                {cardData.map((item, i) => {
                  return <OpenableCard item={item} index={i} key={item.id} />;
                })}
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6 animate__animated animate__fadeIn">
          <img
            src={girlWithLaptop}
            style={{
              width: "-webkit-fill-available",
              position: "relative",
              right: "-10px",
              marginTop: "20px",
              // animation:"fadeInContainer 3s ease-in-out"
            }}
          />
          <div className="container " style={{ paddingLeft: "100px" }}>
            <div className="row acc-det-container ">
              {/* <div className="col-sm accordian-section">gggg</div> */}
              <div className="col-sm course-item-list">
                <div className="course-item-card animate__animated animate__jackInTheBox">
                  {courseCardItems.map((item, index) => {
                    return (
                      <div className="items" key={index}>
                        {item}
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SyllabusSection;
