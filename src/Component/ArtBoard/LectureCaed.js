import React from "react";
import Slider from "react-slick";
function LectureCaed() {
  const centerSetting = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 1000,
    slidesToScroll: 1,
    initialSlide: 0,
    pauseOnHover: true,
    // nextArrow: <SampleNextArrow />,
    // prevArrow: <SamplePrevArrow />,

    responsive: [
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
          infinite: true,
          autoplay: true,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          autoplay: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    // <Slider {...centerSetting}>
    <div className="lecture-card">
      <div className="lecture-card-header">Curt Thomoson</div>
      <div className="lecture-role">PHP Developer</div>
      <div className="lecture-card-image">
        <img src="https://wac-cdn.atlassian.com/dam/jcr:ba03a215-2f45-40f5-8540-b2015223c918/Max-R_Headshot%20(1).jpg?cdnVersion=207" />
      </div>
    </div>
    // </Slider>
  );
}

export default LectureCaed;
