import React from "react";

function CategorySection() {
  let [activeTab, setActiveTab] = React.useState("Beginner");
  const detailItems = [
    { title: "Estimated Time", second: "5 month", third: "At 5-10 hrs/week" },
    {
      title: "ENROLL BY",
      second: "December 1, 2020",
      third: "Get access to classroom immediately on enrollment",
    },
    {
      title: "PREREQUISITES",
      second: activeTab,
      third: "See prerequisites in detail",
    },
  ];
  return (
    <div>
      <div className="choose-category ">
        <div className="choose-category-text animate__animated animate__zoomIn animate__slow">
          Choose Category
        </div>
        <div className="category-item-container display-flex-center-row">
          <div className="container animate__animated animate__fadeInIn animate__slow">
            <div className="row align-items-start">
              {activeTab == "Intermediate" ? (
                <div
                  className="col-sm-4 category-item active display-flex-center-row"
                  id="Intermediate"
                  onClick={() => {
                    setActiveTab("Intermediate");
                  }}
                >
                  Intermediate
                </div>
              ) : (
                <div
                  className="col-sm-4 category-item display-flex-center-row"
                  id="Intermediate"
                  onClick={() => {
                    setActiveTab("Intermediate");
                  }}
                >
                  Intermediate
                </div>
              )}
              {activeTab == "Beginner" ? (
                <div
                  className="col-sm-4 category-item active display-flex-center-row"
                  id="Beginner"
                  onClick={() => {
                    setActiveTab("Beginner");
                  }}
                >
                  Beginner
                </div>
              ) : (
                <div
                  className="col-sm-4 category-item display-flex-center-row"
                  id="Beginner"
                  onClick={() => {
                    setActiveTab("Beginner");
                  }}
                >
                  Beginner
                </div>
              )}
              {activeTab == "Expert" ? (
                <div
                  className="col-sm-4 category-item active display-flex-center-row"
                  id="Expert"
                  onClick={() => {
                    setActiveTab("Expert");
                  }}
                >
                  Expert
                </div>
              ) : (
                <div
                  className="col-sm-4 category-item display-flex-center-row"
                  id="Expert"
                  onClick={() => {
                    setActiveTab("Expert");
                  }}
                >
                  Expert
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      <div class="container details-container display-flex-center-row ">
        <div class="row align-items-start">
          {/* <div className="triangle" /> */}
          {detailItems.map((item, index) => {
            return (
              <div
                className="col detail-item-container display-flex-center-row"
                key={index}
              >
                <div className="detail-item ">
                  <div className="detail-item-text">{item.title}</div>
                  <div className="detail-item-second">{item.second}</div>
                  <div className="detail-item-third">{item.third}</div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}

export default CategorySection;
