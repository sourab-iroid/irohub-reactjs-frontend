import React from "react";
import pythonDevelopment from "../../Asserts/Images/python (1).png";
import phpDevelopment from "../../Asserts/Images/php.png";
import iosDevelopment from "../../Asserts/Images/apple-black-logo.png";
import javaDevelopment from "../../Asserts/Images/java.png";
import androidDevelopment from "../../Asserts/Images/android.png";
import webDevelopment from "../../Asserts/Images/coding.png";
import ScrollAnimation from "react-animate-on-scroll";

function FirstSection() {
  let [courseSectionClicked, setCourseSectionClicked] = React.useState("");
  React.useEffect(() => {
    let courseSection = localStorage.getItem("courseSectionClicked");
    setCourseSectionClicked(courseSection);
  }, []);
  return (
    <div className="artboard-container">
      <div className="course-title-container">
        <div className="course-title display-flex-start">
          {/* <img src={selectedCourseLogo} className="course-logo" />{" "} */}
          {courseSectionClicked === "Python Development" && (
            <img src={pythonDevelopment} className="course-logo" />
          )}
          {courseSectionClicked === "Java Development" && (
            <img src={javaDevelopment} className="course-logo" />
          )}
          {courseSectionClicked === "PHP Development" && (
            <img src={phpDevelopment} className="course-logo" />
          )}
          {courseSectionClicked === "Android Development" && (
            <img src={androidDevelopment} className="course-logo" />
          )}
          {courseSectionClicked === "IOS Development" && (
            <img src={iosDevelopment} className="course-logo" />
          )}
          {courseSectionClicked === "Web Development" && (
            <img src={webDevelopment} className="course-logo" />
          )}

          <span className="course-title-text"> {courseSectionClicked}</span>
        </div>
        <div className="couse-description">
          Laccusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
          no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
          dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
          tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
          voluptua. At vero eos et accusam Laccusam et justo duo dolores et ea
          rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
          ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
          sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
          dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam
        </div>
        <div className="button-container display-flex-start">
          <button className="btn btn-primary enroll-button">Enroll Now</button>
          <button className="btn ms-3 trial-button">Start Free Trail</button>
        </div>
      </div>
      {/* </ScrollAnimation> */}
    </div>
  );
}

export default FirstSection;
