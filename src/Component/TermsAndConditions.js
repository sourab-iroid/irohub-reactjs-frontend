import React, { useEffect } from "react";
import searching from "../Asserts/Privacy/searching.svg";
import divider from "../Asserts/Privacy/divider.svg";
import Aos from "aos";
import "aos/dist/aos.css";

export const TermsAndConditions = () => {
  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);
  return (
    <>
      <div className="container mt-5 terms-conditions">
        <div className="row">
          <div className="col-md-10 mx-auto">
            <div className="row " data-aos="fade-slow">
              <div className="col-md-7 " data-aos="fade-right">
                <h1 className="heading mb-4">Terms And Condition</h1>
                <p className="terms-para">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam voluptua. At vero eos et accusam et
                  justo duo dolores et ea rebum. Stet clita kasd gubergren,
                </p>
                <img src={divider} />
              </div>
              <div className="col-md-5 " data-aos="fade-left">
                <img src={searching} className="img-fluid " />
              </div>
            </div>
            <div className="row">
              <div className="col-md-12 ">
                <div data-aos="fade-up">
                  <h4 className="sub-heading">Updated March 26, 2021</h4>
                  <hr />
                  <p className="terms-para">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua. At vero eos et accusam et
                    justo duo dolores et ea rebum. Stet clita kasd
                    gubergren,Lorem ipsum dolor sit amet, consetetur sadipscing
                    elitr, sed diam nonumy eirmod tempor invidunt ut labore et
                    dolore magna aliquyam erat, sed diam voluptua. At vero eos
                    et accusam et justo duo dolores et ea rebum. Stet clita kasd
                    gubergren,
                  </p>
                </div>

                <div data-aos="fade-up">
                  <h4 className="sub-heading">Something Critical</h4>
                  <hr />
                  <p className="terms-para">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua. At vero eos et accusam et
                    justo duo dolores et ea rebum. Stet clita kasd
                    gubergren,Lorem ipsum dolor sit amet, consetetur sadipscing
                    elitr, sed diam nonumy eirmod tempor invidunt ut labore et
                    dolore magna aliquyam erat, sed diam voluptua. At vero eos
                    et accusam et justo duo dolores et ea rebum. Stet clita kasd
                    gubergren,
                  </p>
                </div>

                <div data-aos="fade-up">
                  <h4 className="sub-heading">Header</h4>
                  <hr />
                  <p className="terms-para">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua. At vero eos et accusam et
                    justo duo dolores et ea rebum. Stet clita kasd
                    gubergren,Lorem ipsum dolor sit amet, consetetur sadipscing
                    elitr, sed diam nonumy eirmod tempor invidunt ut labore et
                    dolore magna aliquyam erat, sed diam voluptua. At vero eos
                    et accusam et justo duo dolores et ea rebum. Stet clita kasd
                    gubergren,
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
