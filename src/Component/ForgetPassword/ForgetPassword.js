import React from "react";
import { OTPModal } from "./OTPModal";

export const ForgetPassword = ({ modalScreen, setModalScreen }) => {
  const [otpOpen, setOtpOpen] = React.useState(false);
  const [newpasswordOpen, setNewpasswordOpen] = React.useState(false);
  let [showOtpModal, setShowOtpModal] = React.useState(false);
  let handleModalOpen = () => {
    setShowOtpModal(!showOtpModal);
    // setModalOpen(false)
    localStorage.setItem("modalOpen", false);
    localStorage.setItem("modalCounter", 2);
    setModalScreen(2);
  };
  React.useEffect(() => {
    let isPassWordOpen = localStorage.getItem("showNewPassword");
    setNewpasswordOpen(isPassWordOpen);
  }, [showOtpModal]);
  return (
    <>
      {/* <button
        type="button"
        className="btn  forgot-pass"
        data-toggle="modal"
        data-target="#forgetModal"
       
      >
        ForgetPassword ?
      </button> */}

      <div
        className="modal fade forget-modal"
        id="forgetModal"
        tabindex="-2"
        // role="dialog"
        aria-labelledby="forgetModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog  " role="document">
          <div className="modal-content">
            <div className="text-center mt-4">
              <h1 className="forget-heading">Forgot your password?</h1>
              <p className="small-text">
                We’ll help you reset it and get back on track.
              </p>
              <div className="mt-4">
                <a className="black-mob">Mobile Number</a>
              </div>
            </div>
            <form className="mt-4">
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">
                  Enter Mobile Number
                </label>
                <input
                  type="email"
                  class="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                />
              </div>
            </form>
            <a
              className="btn submit-btn mt-2"
              data-toggle="modal"
              data-target="#otpModal"
              onClick={handleModalOpen}
            >
              SUBMIT
            </a>
            {/* <OTPModal showOtpModal={showOtpModal} setShowOtpModal={setShowOtpModal} /> */}
            <div className="text-center mt-4 mb-3">
              <a
                type="button"
                className="small-text"
                onClick={() => setModalScreen(0)}
              >
                Back
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
