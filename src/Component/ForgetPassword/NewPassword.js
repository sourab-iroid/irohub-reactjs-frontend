import React from "react";

export const NewPassword = () => {
  return (
    <>
      {/* <button
        type="button"
        className="btn btn-primary"
        data-toggle="modal"
        data-target="#newpasswordModal"
      >
        Launch demo modal
      </button> */}

      <div
        className="modal fade"
        id="newpasswordModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="newpasswordModalLabel"
        aria-hidden="true"
      >
        <div
          className="modal-dialog"
          style={{ marginTop: "8rem", padding: "0 200px", width: "49rem" }}
          role="document"
        >
          <div className="modal-content" style={{ padding: "20px 40px" }}>
            <div className="text-center mt-4">
              <h1
                className="forget-heading"
                style={{
                  marginBottom: 0,
                  font: "600 20px/28px Roboto, Helvetica Neue, sans-serif",
                }}
              >
                Enter your new password
              </h1>
            </div>
            <form className="mt-4">
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">
                  Enter New Password
                </label>
                <input
                  type="password"
                  class="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                />
              </div>
              <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">
                  Confirm Password
                </label>
                <input
                  type="password"
                  class="form-control"
                  id="exampleInputEmail1"
                  aria-describedby="emailHelp"
                />
              </div>
            </form>
            <a
              className="btn submit-btn mt-2"
              data-toggle="modal"
              data-target="#otpModal"
              id="submit-btn"
              style={{
                height: "40px",
                fontWeight: 500,
                fontSize: "14px",
                backgroundColor: "#000",
                color: "white",
              }}
              onClick={() => window.location.reload()}
            >
              SUBMIT
            </a>
          </div>
        </div>
      </div>
    </>
  );
};
