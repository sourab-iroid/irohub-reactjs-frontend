import React, { useState } from "react";
import { NewPassword } from "./NewPassword";
import OTPInput, { ResendOTP } from "otp-input-react";

export const OTPModal = ({ modalScreen, setModalScreen }) => {
  const [OTP, setOTP] = useState("");
  let handleModal = () => {
    setModalScreen(3);
    localStorage.setItem("modalCounter", 3);
  };
  const otpinput = {
    borderRadius: "4px",
    border: "1px solid #c5c5c5",
  };
  return (
    <>
      <div
        className="modal fade forget-modal"
        id="otpModal"
        tabindex="1"
        role="dialog"
        aria-labelledby="otpModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog  " role="document">
          <div className="modal-content">
            <div className="text-center mt-4">
              <h1 className="forget-heading">Enter OTP</h1>
            </div>

            <OTPInput
              value={OTP}
              onChange={setOTP}
              autoFocus
              OTPLength={4}
              otpType="number"
              disabled={false}
              secure
              className="otp-style "
              inputStyles={otpinput}
            />
            {/* <ResendOTP onResendClick={() => console.log("Resend clicked")} /> */}

            <a
              className="btn submit-btn mt-2"
              data-toggle="modal"
              data-target="#newpasswordModal"
              onClick={handleModal}
            >
              SUBMIT
            </a>

            <div className="text-center mt-4 mb-3">
              <a type="button" className="small-text">
                Resend OTP
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
