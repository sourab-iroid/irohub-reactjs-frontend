import React, { useState, useEffect } from "react";
import axios from "axios";
import Group_2779 from "../Asserts/bird/Group_2779.png";
// import { LiveclassNamees } from "./Pages/LiveclassNamees";

import { Header } from "../Common/Header";
import { Footer } from "../Common/Footer";

import Group from "../Asserts/Images/Group.png";
import office from "../Asserts/Images/office.png";
import iosTraining from "../Asserts/Images/ios-training.png";
import { PageHeader } from "../Common/PageHeader";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import Slider from "react-slick";
import {apiUrl} from "../env"
export const PlacementPage = () => {
  const serverURL = "http://13.235.42.102:2000";
  const [companiesData, setCompaniesData] = useState([]);

  let settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    initialSlide: 0,
    autoplay: true,
    autoplaySpeed: 2000,
    pauseOnHover: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  let settings1 = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 6,
    initialSlide: 0,
    autoplay: true,
    autoplaySpeed: 2000,
    pauseOnHover: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  useEffect(() => {
    axios
      .get("http://13.235.42.102:2000/home")
      .then(function (response) {
        //if (response.data.message === "Success")
        response.data.placement_partners.map((data) => {
          console.log(apiUrl + "/" + data.image);
        });

        console.log("sample", response.data.placement_partners);
        setCompaniesData(response.data.placement_partners);
        //console.log("workflowdata", memberData);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);

  return (
    <div className="main">
      {/* <Header /> */}
      {/* <div className="page">
        <div className="banner mb-4"> */}
      <PageHeader pageName="PLACEMENT" />
      <div
        className="container "
        // style={{ position: "relative", bottom: "110px" }}
      >
        <div className="row" style={{ marginBottom: "50px" }}>
          <div className="col-lg-5">
            <div
              className="overlapping-images mb-5 mt-2 wow animate__ animate__fadeIn animate__delay-1s animate__slow  animated"
              style={{ visibility: "visible" }}
            >
              <img
                src={Group}
                alt=""
                className="img-fluid"
                style={{ minWidth: "100%" }}
              />

              {/* </div> */}
            </div>
          </div>
          <div className="col-lg-7">
            <div
              className="side-content mt-5 pl-5 pr-4 wow animate__ animate__fadeIn animate__delay-1s animate__slow  animated"
              style={{
                visibility: "visible",
                position: "relative",
                top: "50px",
              }}
            >
              <h1 className="img-section-heading mb-4">
                About Irohub Placement
              </h1>
              <p className="img-section-para-1">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy{" "}
              </p>
              <p className="img-section-para-2">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
                dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
                eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                sed diam voluptua. At vero eos et accusam et justo duo dolores
                et ea rebum. SteLorem ipsum dolor sit amet, consetetur
                sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                labore et dolore magna aliquyam erat, sed diam voluptua. At vero
                eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
                gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
                amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                takimata sanctus est Lorem ipsum dolor sit amet, consetetur{" "}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid">
        <div className="row">
          <div
            className="col-sm-12 col-md-6"
            style={{ padding: "30px", backgroundColor: "#4668ee" }}
          >
            <h1
              className="eligibility-heading mt-5 wow animate__ animate__zoomIn animate__slow  animated"
              style={{ visibility: "visible", animationName: "zoomInn" }}
            >
              Placement Eligibility
            </h1>
            <p
              className="eligibility-para pr-4 mt-4 mb-4 wow animate__ animate__zoomIn animate__slow  animated"
              style={
                {
                  // visibility: "visible",
                }
              }
            >
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry.Lorem Ipsum has been the and industry's standard dummy
              text ever since the 1500s, when an unknown printer took a galley
              of type scrambled it to make a type specimen book.
            </p>
            <div
              className="lists wow animate__ animate__fadeInLeft animate__slow  animated"
              style={{ visibility: "visible", animationName: "fadeInLeft" }}
            >
              <div className="d-flex justify-content-start mb-4 pl-5">
                <div className="triangle-bullet mr-4"></div>
                <p className="line-list">
                  Lorem Ipsum is simply dummy text of the printing and
                </p>
              </div>
              <div className="d-flex justify-content-start mb-4 pl-5">
                <div className="triangle-bullet mr-4"></div>
                <p className="line-list">
                  Simply dummy text of the printing and
                </p>
              </div>
              <div className="d-flex justify-content-start mb-4 pl-5">
                <div className="triangle-bullet mr-4"></div>
                <p className="line-list">
                  Text of the printing Simply dummy text of the printing and
                </p>
              </div>
              <div className="d-flex justify-content-start mb-4 pl-5">
                <div className="triangle-bullet mr-4"></div>
                <p className="line-list">
                  Text of the printing Simply dummy text of the printing and
                </p>
              </div>
            </div>
          </div>
          <div className="col-sm-12 col-md-6" style={{ padding: "0px" }}>
            <div className="blue-part">
              <div className="text-center">
                <img
                  src={office}
                  className="img-fluid placement3-img wow animate__ animate__fadeInRight animate__slow  animated"
                  style={{
                    visibility: "visible",
                    animationName: "fadeIn",
                  }}
                />
              </div>
            </div>
          </div>
          <div
            className="companies wow animate__ animate__zoomIn animate__slow  animated"
            style={{ visibility: "visible", animationName: "zoomInn" }}
          >
            <h1 className="text-center mt-5 mb-5 company-heading">
              Associated Companies
            </h1>
            <Slider
              {...settings}
              className="owl-theme"
              cellPadding={0}
              animateOut="fadeIn"
              loop
              margin={0}
              dots={false}
              autoPlay={1000}
              autoplayTimeout={1000}
              smartSpeed={1000}
              slideBy="linear"
              autoplayHoverPause={true}
              items={6}
              // autoplaySpeed={3000}
            >
              {companiesData.map((data) => {
                return (
                  <>
                    <div className="item">
                      <img
                        className="companyImage"
                        src={apiUrl + "/" + data.image}
                      />
                    </div>
                  </>
                );
              })}
            </Slider>
          </div>
        </div>
      </div>
      <div className="black-testimonial">
        <div className="test-heading">TESTIMONIALS</div>
        <div className="red-vl"></div>
      </div>
      {/* <h1 style={{ paddingRight: "200px" }}>kedjewjdwjsiw</h1> */}
      <div style={{ marginLeft: "200px" }}>
        <Slider
          {...settings1}
          className="owl-theme"
          cellPadding={0}
          animateOut="fadeIn"
          loop
          margin={0}
          dots={false}
          autoPlay={1000}
          autoplayTimeout={1000}
          smartSpeed={1000}
          slideBy="linear"
          autoplayHoverPause={true}
          items={6}
        >
          <div className="item">
            <div>
              <img src={iosTraining} style={{ width: "80%" }} />
            </div>
            <div style={{ width: "80%" }}>
              <p className="img-section-para-2">
                We are a small but efficient team of passionated designers
                believing there are no boundaries between form and function, and
                no border between graphic design and user experience.
              </p>
            </div>
          </div>
          <div className="item">
            <div>
              <img src={iosTraining} style={{ width: "80%" }} />
            </div>
            <div style={{ width: "80%" }}>
              <p className="img-section-para-2">
                We are a small but efficient team of passionated designers
                believing there are no boundaries between form and function, and
                no border between graphic design and user experience.
              </p>
            </div>
          </div>
          <div className="item">
            <div>
              <img src={iosTraining} style={{ width: "80%" }} />
            </div>
            <div style={{ width: "80%" }}>
              <p className="img-section-para-2">
                We are a small but efficient team of passionated designers
                believing there are no boundaries between form and function, and
                no border between graphic design and user experience.
              </p>
            </div>
          </div>
          <div className="item">
            <div>
              <img src={iosTraining} style={{ width: "80%" }} />
            </div>
            <div style={{ width: "80%" }}>
              <p className="img-section-para-2">
                We are a small but efficient team of passionated designers
                believing there are no boundaries between form and function, and
                no border between graphic design and user experience.
              </p>
            </div>
          </div>
        </Slider>
      </div>
      {/* <Footer /> */}
      {/* </div>
      </div> */}

      {/* </div> */}
      {/* <Footer /> */}
    </div>
  );
};
