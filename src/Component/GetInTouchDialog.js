import React, { useState } from "react";
import IrohubLogo from "../Asserts/logo/IrohubLogo.png";
import { AiFillCloseCircle } from "react-icons/ai";
import { AiOutlineClose } from "react-icons/ai";
import { FaFacebookF } from "react-icons/fa";
import { AiOutlineInstagram } from "react-icons/ai";
import { AiOutlineTwitter } from "react-icons/ai";
import { FaLinkedinIn } from "react-icons/fa";

export const GetInTouchDialog = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [message, setMessage] = useState("");

  const [nameErr, setNameErr] = useState({});
  const [emailErr, setEmailErr] = useState({});
  const [numberErr, setNumberErr] = useState({});
  const [messageErr, setMessageErr] = useState({});

  const onSubmit = (e) => {
    e.preventDefault();
    const isValid = formValidation();
  };

  const formValidation = () => {
    const nameErr = {};
    const emailErr = {};
    const numberErr = {};
    const messageErr = {};
    // const { nameErr, emailErr, numberErr } = "";
    let isValid = true;

    // if (name.trim().length < 5) {
    //   nameErr.nameEmpty = "pleae Enter the name";
    // }
    if (name.trim() === "") {
      nameErr.nameEmpty = "pleae Enter the Name";
    }
    if (email.trim() === "") {
      emailErr.emailEmpty = "pleae Enter the Email";
    }
    if (mobileNumber.trim() === "") {
      numberErr.mobileNumberEmpty = "pleae Enter the Mobile Number";
    }
    if (message.trim() === "") {
      messageErr.messageEmpty = "pleae Enter the Message";
    }
    setNameErr(nameErr);
    setEmailErr(emailErr);
    setNumberErr(numberErr);
    setMessageErr(messageErr);
  };

  const iconStyle = {
    height: "30px",
    width: "30px",
    color: "#fff",
    right: "25px",
    position: "absolute",
    top: "10px",
    cursor: "pointer",
  };
  return (
    <>
      <div
        className="modal fade getintouch-dialogStyle"
        id="contactModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="row relative">
              <div className="col-sm-6 col-md-6 black-part">
                <div class=" phone-display">
                  <button
                    type="button"
                    class=" phone-btn btn  ms-auto "
                    data-dismiss="modal"
                  >
                    <AiFillCloseCircle className="icon" />
                  </button>
                </div>
                <img src={IrohubLogo} className="logo" />
                <h1 className="about-head">About Irohub</h1>
                <p className="about-para">
                  Lorem ipsum dolor sit amet, consetetur sadipscing
                </p>
                <p className="about-para">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam voluptua. At vero eos et accusam et
                  justo duo dolores et ea rebum. Stet clita kasd gubergren, no
                  sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem
                  ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                  nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam voluptua. At vero eos et accusam et
                  justo duo dolores et ea rebum. Stet clita kasd gubergren, no
                  sea takimata sanctus est
                </p>
                <div className="d-flex justify-content-center ">
                  <hr className="w-25 mt-3" />
                  <div className="icon-style text-center ">
                    <ul className="d-inline-flex ">
                      <li>
                        <a
                          href="#"
                          type="button"
                          className="social-btns mat-raised-button shadow "
                        >
                          <FaFacebookF className="icon-style1" />
                        </a>
                      </li>
                      <li>
                        <a
                          href="#"
                          type="button"
                          className="social-btns mat-raised-button shadow ms-2"
                        >
                          <AiOutlineInstagram className="icon-style1" />
                        </a>
                      </li>
                      <li>
                        <a
                          href="#"
                          type="button"
                          className="social-btns mat-raised-button shadow ms-2"
                        >
                          <AiOutlineTwitter className="icon-style1" />
                        </a>
                      </li>
                      <li>
                        <a
                          href="#"
                          type="button"
                          className="social-btns mat-raised-button shadow ms-2"
                        >
                          <FaLinkedinIn className="icon-style1" />
                        </a>
                      </li>
                    </ul>
                  </div>
                  <hr className="w-25 mt-3" />
                </div>
              </div>
              <div className="col-sm-6 col-md-6 main-part text-white">
                <div className="mx-4">
                  <div class="modal-header mt-2">
                    <button
                      type="button"
                      class=" touch-btn btn b-0 ms-auto"
                      data-dismiss="modal"
                    >
                      {/* <AiOutlineClose className="icon" /> */}
                      <AiOutlineClose style={iconStyle} />
                    </button>
                  </div>
                  <h1 className="talk-section">Let' talk.</h1>
                  <p className="talk-text">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy
                  </p>
                  <div>
                    <form onSubmit={onSubmit}>
                      <div className="mb-3">
                        <label>Name</label>
                        <br />
                        <input
                          type="name"
                          className="no-outline "
                          value={name}
                          onChange={(e) => setName(e.target.value)}
                        />
                        {Object.keys(nameErr).map((key) => {
                          return (
                            <div style={{ color: "red" }}>{nameErr[key]}</div>
                          );
                        })}
                      </div>
                      <div className="mb-3">
                        <label>Email</label>
                        <br />
                        <input
                          type="email"
                          className="no-outline "
                          value={email}
                          onChange={(e) => setEmail(e.target.value)}
                        />
                        {Object.keys(emailErr).map((key) => {
                          return (
                            <div style={{ color: "red" }}>{emailErr[key]}</div>
                          );
                        })}
                      </div>
                      <div className="mb-3">
                        <label>Contact Number</label>
                        <br />
                        <input
                          type="number"
                          className="no-outline "
                          value={mobileNumber}
                          onChange={(e) => setMobileNumber(e.target.value)}
                        />
                        {Object.keys(numberErr).map((key) => {
                          return (
                            <div style={{ color: "red" }}>{numberErr[key]}</div>
                          );
                        })}
                      </div>
                      <div className="mb-3">
                        <label>Message</label>
                        <br />
                        <textarea
                          type="text"
                          className="no-outline"
                          rows="3"
                          value={message}
                          onChange={(e) => setMessage(e.target.value)}
                        />
                        {Object.keys(messageErr).map((key) => {
                          return (
                            <div style={{ color: "red" }}>
                              {messageErr[key]}
                            </div>
                          );
                        })}
                      </div>
                      <button type="submit" className="btn ">
                        Send Message
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
