import React from "react";
import university from "../../../Asserts/Images/university.png";

export const RegistrationSection = () => {
  return (
    <div>
      <div className="registration-section wow animate__ animate__fadeIn animate__slow  animated">
        <div className="registration">
          <div className="container reg">
            <img src={university} className="req-video " data-aos="zoom-in" alt="university" />
          </div>
        </div>
      </div>
    </div>
  );
};
