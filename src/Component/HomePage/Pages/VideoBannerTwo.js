import React from "react";
import circle_top_right from "../../../Asserts/Images/circle_top_right.png";
import workplace_banner from "../../../Asserts/Images/workplace_banner.png";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";

export const VideoBannerTwo = () => {
  return (
    <>
      <div className="videobanner-two">
        <div className="left-bottom-triangle"></div>
        <div className="main-shape"></div>
        <div className="left-top-triangle"></div>
        <img
          src={circle_top_right}
          className="circle-dot"
          alt="circle_top_right"
        />
        <img
          src={workplace_banner}
          className="img-shape"
          alt="workplace_banner"
        />
        <div className="text-content mt-3">
          <h1 className="heading text-white">Project Ideas</h1>
          <p className="para text-white">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type
          </p>
          <button
            className="btn learn-more-btn"
            onClick={() => window.location.assign("/videoClasses")}
          >
            <span className="btn-text">Learn More</span>
            <HiOutlineArrowNarrowRight className="btn-arrow-icon ms-3" />
          </button>
        </div>
        <div className="project-ideas-top"></div>
        <div className="main-line"></div>
      </div>
    </>
  );
};
