import React from "react";
import workplace_bg from "../../../Asserts/Images/workplace_bg.png";
import workplace from "../../../Asserts/Images/workplace.png";
import meeting from "../../../Asserts/Images/meeting.png";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";

export const WorkTogether = () => {
  return (
    <>
      <div className="container-fluid  work-together" data-aos="fade-up">
        <div className="project ">
          <div className="row">
            <div className="col-md-6 mt-5">
              <div className="shape-img " data-aos="fade-right">
                <div className="square-shape mt-4">
                  <img src={workplace_bg} className=" img-fluid wt-image" alt="workplace_bg" />
                </div>
                <div className="work-img">
                  <img src={workplace} className="img-fluid wt-image1" alt="workplace" />
                </div>
              </div>
            </div>
            <div className="col-md-6" data-aos="zoom-in">
              <div className="project-diescrption">
                <p className="project-subhead"> Lorem Ipsum s simply dummy </p>
                <h1 className="project-head">Bring All Your Work Together</h1>
                <p className="project-para">
                  Lorem Ipsum is simply dummy text of the printing an d
                  typesetting industry. Lorem Ipsum has been the indu stry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to m ake a type
                  specimen book. Ipsum is simply dummy text of the prin ting and
                  typeset ting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s,
                </p>
                <div className="meeting-div mt-5">
                  <img src={meeting} className="meeting" alt="meeting" />
                  <span className="ms-5">
                    <button className="learn-more-btn">
                      {" "}
                      <span>Learn More</span>{" "}
                      <HiOutlineArrowNarrowRight className="btn-arrow-icon ms-2" />
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
