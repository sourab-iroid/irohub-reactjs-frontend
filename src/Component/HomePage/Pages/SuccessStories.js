import React, { useState, useEffect } from "react";
import axios from "axios";
import profile from "../../../Asserts/Images/profile.png";
import Slider from "react-slick";
import { FaQuoteLeft } from "react-icons/fa";
import { FaQuoteRight } from "react-icons/fa";
import { apiUrl } from "../../../env";

export const SuccessStories = () => {
  const serverURL = "http://13.234.231.243:2000";

  const [profileData, setProfileData] = useState([]);

  useEffect(() => {
    console.log("place");
    axios
      .get(`${apiUrl}/home`)
      .then(function (response) {
        //if (response.data.message === "Success")
        response.data.success_stories.map((data) => {
          console.log(apiUrl + "/" + data.image);
        });

        console.log("sample", response.data.success_stories);
        setProfileData(response.data.success_stories);
        //console.log("workflowdata", memberData);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);

  const centerSetting = {
    dots: true,
    infinite: true,
    slidesToShow: 3,
    autoplay: true,
    autoplaySpeed: 1000,
    slidesToScroll: 1,

    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          autoplay: true,
        },
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <>
      <div className="container success-stories" data-aos="zoom-in">
        <h1 className="succcess-head text-center">Success Stories</h1>
        <div className="row">
          <Slider {...centerSetting} className="slickstyle">
            {[1, 2, 3, 4, 5, 6].map(() => {
              return (
                <>
                  {profileData.map((item) => {
                    return (
                      <div className="col-md-4 story-card">
                        <div className="text-center ">
                          <img
                            className="profile text-center"
                            src={apiUrl + "/" + item.image}
                            alt="profile"
                          />
                        </div>
                        <div className="card  story-head shadow">
                          <div className="card-body text-center mt-4">
                            <h3 className="card-title fs-6 fw-bold">
                              {item.name}
                            </h3>
                            <p className="para fs-6">{item.position}</p>
                            <FaQuoteLeft />
                            <p className="card-text  success-para">
                              {item.quote}
                            </p>
                            <FaQuoteRight />
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </>
              );
            })}
          </Slider>
        </div>
      </div>
    </>
  );
};
