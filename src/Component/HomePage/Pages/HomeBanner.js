import React from "react";
import Slider from "react-slick";
import { MobileHomePageBanner } from "./ResponsiveHomePage/MobileHomePageBanner";
import { VideoBanner } from "./VideoBanner";
import { VideoBannerFive } from "./VideoBannerFive";
import { VideoBannerFour } from "./VideoBannerFour";
import { VideoBannerThree } from "./VideoBannerThree";
import { VideoBannerTwo } from "./VideoBannerTwo";

export const HomeBanner = () => {
  const bannerSetting = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToScroll: 1,
  };
  return (
    <>
      <div className="home_banner mb-5">
        <Slider {...bannerSetting} className="slide_banner">
          <VideoBanner />
          <VideoBannerTwo />
          <VideoBannerThree />
          <VideoBannerFour />
          <VideoBannerFive label="Learn More" />
        </Slider>
      </div>

      {/* <div className="mobile-display">
        <MobileHomePageBanner />
      </div> */}
    </>
  );
};
