import React from "react";
import scholarship from "../../../Asserts/Images/scholarship.png";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";
import { ScholarshipDialog } from "../../ScholarshipDialog";
export const VideoBannerFive = ({ label }) => {
  React.useEffect(() => {
    if (label == "Apply Now") {
      let scholarshipButton = document.getElementById("scholarshipButton");
      scholarshipButton.setAttribute("data-toggle", "modal");
      scholarshipButton.setAttribute("data-target", "#scholarshipModal");
    }
  }, []);
  let scholarshipButton = () => {
    if (label == "Learn More") {
      window.location.assign("/scholarship");
    }
  };
  return (
    <>
      <div className=" videobanner-five" data-aos="fade-up">
        <div className="scholarship-shape1">
          <div className="scholorship-banner-title text-white">
            <h1 className="scholorship-banner-head">Scholarship</h1>

            <p className="scholorship-banner-para">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et accusam et justo duo
              dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
              sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
              amet,{" "}
            </p>
            <div className="btn-scholar">
              <button
                className="btn learn-more-btn"
                id="scholarshipButton"
                onClick={scholarshipButton}
              >
                <span className="btn-text mt-2">{label}</span>
                <HiOutlineArrowNarrowRight className="btn-arrow-icon ms-3" />
              </button>
            </div>
          </div>
          <div className="scholarship-shape2"></div>
          <div className="scholarship-shape3"></div>
        </div>
        <div className="scholoarship-shape4"></div>
        <div className="d-flex justify-content-end shape2">
          <img
            src={scholarship}
            alt="scholarship"
            className="scholoarship-banner-img img-fluid"
          />
        </div>
      </div>
    </>
  );
};
