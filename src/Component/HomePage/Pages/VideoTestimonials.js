import React, { useState, useEffect } from "react";
import axios from "axios";
import line_img1 from "../../../Asserts/Images/line_img1.png";
import line_img2 from "../../../Asserts/Images/line_img2.png";
import video2 from "../../../Asserts/Images/video2.png";
import video3 from "../../../Asserts/Images/video3.png";
import { apiUrl } from "../../../env";
import { BsStarHalf } from "react-icons/bs";
import Slider from "react-slick";

export const VideoTestimonials = () => {
  let rateData;
  const [data, setData] = React.useState();
  useEffect(() => {
    console.log("place");
    axios
      .get(`${apiUrl}/home`)
      .then(function (response) {
        console.log("Response", response);
        rateData = [
          {
            id: 1,
            rate_count: response.data.downloads,
            rate_details: "Downloads",
          },
          {
            id: 2,
            rate_count: response.data.projects,
            rate_details: "Projects",
          },
          {
            id: 3,
            rate_count: response.data.classes,
            rate_details: "Classes",
          },
          {
            id: 4,
            rate_count: response.data.downloads,
            rate_details: "Downloads",
          },
        ];
        setData(rateData);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);
  // function SampleNextArrow(props) {
  //   const { className, style, onClick } = props;
  //   return (
  //     <div
  //       className={className}
  //       style={{ ...style, display: "block", background: "red" }}
  //       onClick={onClick}
  //     />
  //   );
  // }

  // function SamplePrevArrow(props) {
  //   const { className, style, onClick } = props;
  //   return (
  //     <div
  //       className={className}
  //       style={{
  //         ...style,
  //         display: "block",
  //         background: "green",
  //       }}
  //       onClick={onClick}
  //     />
  //   );
  // }

  const centerSetting = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 6,
    autoplay: true,
    autoplaySpeed: 1000,
    slidesToScroll: 1,
    initialSlide: 0,
    pauseOnHover: true,
    // nextArrow: <SampleNextArrow />,
    // prevArrow: <SamplePrevArrow />,

    responsive: [
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
          infinite: true,
          autoplay: true,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          autoplay: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const [downloadData, setDownloadData] = useState([]);

  useEffect(() => {
    axios
      .get("http://13.235.42.102:2000/home")
      .then(function (response) {
        //if (response.data.message === "Success")
        response.data.testimonials.map((data) => {
          console.log(apiUrl + "/" + data.image);
        });

        console.log("projectsdata", response.data.testimonials);
        setDownloadData(response.data.testimonials);
        //console.log("workflowdata", downloadData);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);

  return (
    <>
      <div className="video-testimon" data-aos="fade-up ">
        <div className="container-fluid">
          <div className="video-shape1"></div>
          <div className="video-shape2">
            <img src={line_img1} className="line-img1" alt="line_img1" />
            <img src={line_img2} className="line-img2" alt="line_img2" />
            <div className="values">
              <div className="container">
                <div className="row">
                  {data &&
                    data.map((item) => {
                      return (
                        <div className="col col-sm-3 col-md-3">
                          <div className="half-star text-white">
                            <BsStarHalf />
                            <h1 className="d-head text-center">
                              {item.rate_count}
                            </h1>
                            <h3 className="d-head text-center">
                              {item.rate_details}
                            </h3>
                          </div>
                        </div>
                      );
                    })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="video-background">
        <div className="container" data-aos="fade-up ">
          <div className="testimonial-shape1">
            <div className="videos">
              <h1 className="test-head text-center text-white fs-2 fw-bold">
                Video Testimonials
              </h1>
              <Slider {...centerSetting} className="slickstyle">
                {downloadData.map((item) => {
                  return (
                    <>
                      <div className="testimonial-img  ms-3">
                        <img
                          src={apiUrl + "/" + item.image}
                          className="img-style"
                          alt="video3"
                        />
                      </div>
                    </>
                  );
                })}
              </Slider>
            </div>
            {/* <div className="polygen"></div> */}
          </div>
        </div>
      </div>
    </>
  );
};
