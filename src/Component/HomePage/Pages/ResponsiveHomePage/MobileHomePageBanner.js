import React from "react";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";

export const MobileHomePageBanner = () => {
  return (
    <>
      <div className="container-fluid">
        <div className="regional-shape">
          <div className="regional-content text-white mt-4">
            <h1 className="regional-head">Video Classes </h1>
            <p className="regional-para mt-3">
              {" "}
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type
            </p>

            <button className="btn learn-more-btn mt-3">
              <span className="btn-text">Learn More</span>
              <HiOutlineArrowNarrowRight className="btn-arrow-icon ms-3" />
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
