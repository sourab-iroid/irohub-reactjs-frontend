import React, { useState, useEffect } from "react";
import axios from "axios";
import Slider from "react-slick";
import { apiUrl } from "../../../env";

// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";

export const PlacementPartners = () => {
  const serverURL = "http://13.234.231.243:2000";
  // const settings = {
  //   dots: true,
  //   infinite: true,
  //   speed: 500,
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  // };
  const autosetting = {
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1000,
  };
  let settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5,
    initialSlide: 0,
    autoplay: true,
    autoplaySpeed: 2000,
    pauseOnHover: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const [placementData, setPlacementData] = useState([]);

  useEffect(() => {
    console.log("place");
    axios
      .get(`${apiUrl}/home`)
      .then(function (response) {
        //if (response.data.message === "Success")
        response.data.placement_partners.map((data) => {
          console.log(apiUrl + "/" + data.image);
        });

        console.log("sample", response.data.placement_partners);
        setPlacementData(response.data.placement_partners);
        //console.log("workflowdata", memberData);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);

  return (
    <>
      <div className="container-fluid " data-aos="fade-slow">
        <div className="placement">
          <h2 className="logo-head text-center"> Our Placement Partners </h2>
          <div className="pb-5 ps-5">
            <Slider {...settings}>
              {placementData.map((item) => {
                // console.log("hello",item);
                return (
                  <div className="imgstyle">
                    <img
                      src={apiUrl + "/" + item.image}
                      className="img-fluid imgstyle"
                      alt="partners_1"
                    />
                  </div>
                );
              })}
            </Slider>
          </div>
        </div>
      </div>
    </>
  );
};
