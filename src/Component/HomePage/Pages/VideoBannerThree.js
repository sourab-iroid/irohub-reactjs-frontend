import React from "react";
import java from "../../../Asserts/Images/java.png";
import phython from "../../../Asserts/Images/python.png";
import android from "../../../Asserts/Images/android.png";
import home_labtop from "../../../Asserts/Images/home_labtop.png";
import circle_top_right from "../../../Asserts/Images/circle_top_right.png";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";

export const VideoBannerThree = () => {
  return (
    <>
      <div className=" videobanner-three videobanner">
        <div className="live-banner"></div>

        <div className="live-shape1"></div>

        <div className="live-shape2"></div>
        <div className="live-shape5"></div>
        <div className="live-shape3"></div>

        <div className="live-shape7"></div>
        <div className="live-shape8"></div>
        <div className="text-content text-white">
          <h1 className="heading">Live Classes</h1>
          <p className="para">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type
          </p>
          <button
            className="btn learn-more-btn"
            onClick={() => window.location.assign("/videoClasses")}
          >
            <span className="btn-text">Learn More</span>
            <HiOutlineArrowNarrowRight className="btn-arrow-icon ms-3" />
          </button>
        </div>

        <img src={java} className="course-logo java" alt="java" />
        <img src={phython} className="course-logo phython" alt="phython" />
        <img src={android} className="course-logo android" alt="android" />
        <img src={home_labtop} className="laptop" alt="home_labtop" />
        <img
          src={circle_top_right}
          className="circle-dot"
          alt="circle_top_right"
        />
      </div>
    </>
  );
};
