import React from "react";
import live_class1 from "../../../Asserts/Images/live_class1.png";
import video_class from "../../../Asserts/Images/video_class.png";
import { MdDesktopMac } from "react-icons/md";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";

export const LiveClasses = () => {
  return (
    <>
      <div className="live-video  mb-5" data-aos="fade-up fade-slow">
        <div className="left-img " data-aos="fade-right">
          <img
            src={live_class1}
            alt="live_class"
            className="img-fluid startup"
          />
          <div className="live-line-shape"></div>
        </div>
        <div className="right-img " data-aos="fade-left">
          <img
            src={video_class}
            className="img-fluid glass-lady"
            alt="video_class"
          />
          <div className="right-img-shape"></div>
        </div>
        <div className="container">
          <div className="live-class-des " data-aos="zoom-in">
            <h2 className="live-class-title">
              <MdDesktopMac /> Live classes
            </h2>
            <p className="live-class-content">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. Lorem Ipsum is
              simply dummy text of the printing and typesetting industry. Lorem
              Ipsum has been the industry's standard dummy text ever since the
              1500s, when an unknown printer took a galley of type and scrambled
              it to make a type specimen book.
            </p>
            <div className="text-end">
              <a href="/liveClasses">
                <button className="btn learn-more-btn">
                  <span className="btn-text">Learn More</span>

                  <HiOutlineArrowNarrowRight className="btn-arrow-icon" />
                </button>
              </a>
            </div>
          </div>
          <div className=" video-class-des mt-3" data-aos="zoom-in">
            <h2 className="video-class-des-title">
              <MdDesktopMac /> Video classes
            </h2>
            <p className="video-class-des-content">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. Lorem Ipsum is
              simply dummy text of the printing and typesetting industry. Lorem
              Ipsum has been the industry's standard dummy text ever since the
              1500s, when an unknown printer took a galley of type and scrambled
              it to make a type specimen book.
            </p>
            <div className="text-end">
              <a href="/videoClasses">
                <button className="btn learn-more-btn">
                  <span className="btn-text">Learn More</span>
                  <HiOutlineArrowNarrowRight className="btn-arrow-icon" />
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
