import React from "react";
import business1 from "../../../Asserts/Images/business1.png";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";

export const Scholorship = () => {
  return (
    <>
      <div className="scholarship mt-5 mb-5" data-aos="fade-up">
        <div data-aos="fade-right">
          <img src={business1} className="ss" alt="business1" />
        </div>
        <div className="right-img ">
          <div className="right-lshape"></div>
          <div className="ssright-triangle"></div>
          <div className="ssbottom-triangle"></div>
          <div className="left-triangle"></div>
        </div>
        <div className="container" data-aos="zoom-in">
          <div className="content ">
            <h2>Scholarship</h2>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. Lorem Ipsum is
              simply dummy text of the printing and typesetting industry. Lorem
              Ipsum has been the industry's standard dummy text ever since the
              1500s, when an unknown printer took a galley of type and scrambled
              it to make a type specimen book. Lorem Ipsum is simply dummy text
              of the printing and typesetting industry. Lorem Ipsum has been the
              industry's standard dummy text ever since the 1500s,
            </p>
            <div className="text-end">
              <a href="/scholarship">
                {" "}
                <button className="btn learn-more-btn">
                  <span className="btn-text">Learn More</span>
                  <HiOutlineArrowNarrowRight className="btn-arrow-icon" />
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>

      {/* <div className="container scholarship">
        <div className="row">
          <div className="col-md-1">
            <img src={business1} className="img-fluid ss" alt="business1" />
          </div>
          <div className="col-md-8">
            <div className="content ">
              <h2>Scholarship</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. Lorem
                Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. Lorem
                Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s,
              </p>
              <div className="text-end">
                <button className="btn learn-more-btn">
                  <span className="btn-text">Learn More</span>
                  <HiOutlineArrowNarrowRight className="btn-arrow-icon" />
                </button>
              </div>
            </div>
          </div>
          <div className="col-md-1">
            <div className="right-img ">
              <div className="right-lshape"></div>
              <div className="ssright-triangle"></div>
              <div className="ssbottom-triangle"></div>
              <div className="left-triangle"></div>
            </div>
          </div>
        </div>
      </div> */}
    </>
  );
};
