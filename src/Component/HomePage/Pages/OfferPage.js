import React from "react";
import phython_training from "../../../Asserts/Images/phython_training.png";
import java_training from "../../../Asserts/Images/java_training.png";
import android_training from "../../../Asserts/Images/android_training.png";
import ios_training from "../../../Asserts/Images/ios_training.png";
import php_training from "../../../Asserts/Images/php_training.png";

export const OfferPage = () => {
  return (
    <div className="mt-5 what-we-offer " data-aos="fade-up fade-slow">
      <div className="offer-left-tri "></div>
      <div className="offer-left-tri2 "></div>
      <div className="container">
        <div className="row">
          <div className="col-md-6" data-aos="fade-right">
            <h1 className="courses-heading mt-4 mb-4 "> What we offer</h1>
            <p className="courses-para pr-5 mb-3 ">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been industry's standard dummy text ever
              since the 1500s, when an unknown printer took a galley type and
              scrambled it to make a type specimen book.
            </p>
          </div>
          <div className="col-sm-3 col-md-3 offer-page" data-aos="fade-left">
            <div className="text-center training ">
              <img
                src={phython_training}
                alt="phython_training"
                className="img-fluid"
              />
              <button
                className="training-btn"
                // style={{ position: "absolute", left: "45rem", top: "11rem" }}
              >
                Python Training
              </button>
            </div>
          </div>
          <div className="col-sm-3 col-md-3 offer-page" data-aos="fade-left">
            <div className="text-center training ">
              <img
                src={java_training}
                alt="java_training"
                className="img-fluid"
              />
              <button
                className="training-btn"
                // style={{ position: "absolute", left: "63rem", top: "11rem" }}
              >
                Java Training
              </button>
            </div>
          </div>
        </div>
        <div
          className="row justify-content-end mt-4 offer"
          data-aos="fade-left"
        >
          <div className="col-sm-3 col-md-3">
            <div className="text-center training1 ">
              <img
                src={android_training}
                alt="android_training"
                className="img-fluid"
              />
              <button
                className="training-btn"
                // style={{ position: "absolute", left: "27rem", bottom: "1rem" }}
              >
                Android Training
              </button>
            </div>
          </div>
          <div className="col-sm-3 col-md-3">
            <div className="text-center training1 ">
              <img
                src={ios_training}
                alt="ios_training"
                className="img-fluid"
              />
              <button
                className="training-btn"
                // style={{ position: "absolute", left: "45rem", bottom: "1rem" }}
              >
                IOS Training
              </button>
            </div>
          </div>
          <div className="col-sm-3 col-md-3">
            <div className="text-center training1 ">
              <img
                src={php_training}
                alt="php_training"
                className="img-fluid"
              />
              <button
                className="training-btn"
                // style={{ position: "absolute", left: "63rem", bottom: "1rem" }}
              >
                PHP Training
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
