import React from "react";
import Slider from "react-slick";
import { OurFacultyCarousal } from "./OurFacultyCarousal";

export const OurFaculty = () => {
  const facultySetting = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToScroll: 1,
  };
  return (
    <>
      <div className="container-fluid " data-aos="fade-up">
        <div className="faculty-background">
          <div className=" faculty-shape">
            <Slider {...facultySetting} className="slide_style">
              {[1, 2, 3].map(() => {
                return (
                  <>
                    <OurFacultyCarousal />
                  </>
                );
              })}
            </Slider>
            {/* <OurFacultyCarousal /> */}
          </div>
          <div className="faculty-shape2"></div>
        </div>
      </div>
    </>
  );
};
