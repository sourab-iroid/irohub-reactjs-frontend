import React from "react";
import java from "../../../Asserts/Images/java.png";
import phython from "../../../Asserts/Images/python.png";
import android from "../../../Asserts/Images/android.png";
import home_labtop from "../../../Asserts/Images/home_labtop.png";
import circle_top_right from "../../../Asserts/Images/circle_top_right.png";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";

export const VideoBanner = () => {
  return (
    <>
      <div className="videobanner">
        <div className="left-triangle"> </div>
        <div className="main-shape"></div>
        <div>
          <img src={java} className="course-logo java" alt="java" />
          <img src={phython} className="course-logo phython" alt="phython" />
          <img src={android} className="course-logo android" alt="android" />
          <img src={home_labtop} className="laptop" alt="home_laptop" />
          <img
            src={circle_top_right}
            className="circle-dot"
            alt="circle_top_right"
          />
        </div>

        <div className="video-small-tritop"></div>
        <div className="video-small-tribot"></div>
        <div className="text-content">
          <h1 className="heading text-white">Video Classes</h1>
          <p className="para text-white">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type
          </p>
          <button
            className="btn learn-more-btn"
            onClick={() => window.location.assign("/videoClasses")}
          >
            <span className="btn-text">Learn More</span>
            <HiOutlineArrowNarrowRight className="btn-arrow-icon ms-3" />
          </button>
        </div>
        <div className="right-triangle"></div>
        <div className="video-shape4"></div>
      </div>
      <div className="video-class-top"></div>
    </>
  );
};
