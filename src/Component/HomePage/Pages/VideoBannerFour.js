import React from "react";
import regional from "../../../Asserts/Images/regional.png";

export const VideoBannerFour = () => {
  return (
    <>
      <div className="videobanner-four">
        <div className="regional-shape">
          <div className="regional-content">
            <h1 className="regional-head">Regional Language </h1>
            <p className="regional-para">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, w hen an unknown printer took a galley of
              type and scrambled it to
            </p>
          </div>
        </div>
        <img
          src={regional}
          className="img-fluid regional-banner"
          alt="regional"
        />
      </div>
    </>
  );
};
