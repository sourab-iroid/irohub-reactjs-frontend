import React from "react";
import woman_sitting from "../../../Asserts/Images/woman_sitting.png";
import { FaQuoteLeft } from "react-icons/fa";
import { FaQuoteRight } from "react-icons/fa";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";

export const InterviewSchedule = () => {
  return (
    <>
      <div className="interview-schedule mt-5 " data-aos="fade-up fade-slow">
        <div className="left-img " data-aos="fade-right">
          <img
            src={woman_sitting}
            className="img-fluid women_sitting"
            alt="woman_sitting"
          />
        </div>
        <div className="titles " data-aos="flip-left">
          <h4 className="font-weight-bold interview-para1">
            Not sure where to start with?
          </h4>
          <h1 className="font-weight-bold interview-para2 mt-4">
            Not sure where to start with? <br /> Schedule an interview with us
          </h1>
        </div>
        <div className=" orange-triangle"></div>
        <div data-aos="zoom-in" className="orange-card card  mt-2">
          <div className="card-body">
            <div className="text-white fs-3">
              <FaQuoteLeft />
            </div>
            <p data-aos="fade-up" className="mt-2 mb-2 text-white ms-3 ">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. Lorem Ipsum is
              simply dummy text of the printing and typesetting industry. Lorem
              Ipsum has been the industry's standard dummy text ever since the
              1500s,
            </p>
            <div className="text-end text-white fs-3">
              <FaQuoteRight />
            </div>
            <div className="text-center">
              <button className="btn learn-more-btn">
                <span className="fs-6 fw-bold">Learn More</span>
                <HiOutlineArrowNarrowRight className="fs-4 ms-2 mb-1" />
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
