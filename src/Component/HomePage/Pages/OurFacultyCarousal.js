import React, { useState, useEffect } from "react";
import { FaFacebookF } from "react-icons/fa";
import { AiOutlineInstagram } from "react-icons/ai";
import { AiOutlineTwitter } from "react-icons/ai";
import { FaLinkedinIn } from "react-icons/fa";
import faculty_member from "../../../Asserts/Images/faculty_member.png";
import axios from "axios";
import { apiUrl } from "../../../env";

export const OurFacultyCarousal = () => {
  const serverURL = "http://13.234.231.243:2000";

  const [facultyData, setFacultyData] = useState([]);

  useEffect(() => {
    console.log("place");
    axios
      .get(`${apiUrl}/home`)
      .then(function (response) {
        //if (response.data.message === "Success")
        response.data.our_faculties.map((data) => {
          console.log(apiUrl + "/" + data.image);
        });

        console.log("sample", response.data.our_faculties);
        setFacultyData(response.data.our_faculties);
        //console.log("workflowdata", memberData);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);

  
  return (
    <div className="container">
      <div className=" row">
        {facultyData.map((item) => {
          return (
            <>
              <div className="col-md-12 col-lg-7 col-xl-7">
                <h1 className="faculty-head text-center mt-5">Our Faculty</h1>
                <div className="faculty-name"> {item.name}</div>
                <p className="faculty-para">{item.description}</p>
              </div>
              <div className="col-lg-5 col-xl-5 mt-5">
                <img
                  src={apiUrl + "/" + item.image}
                  className="faculty_img"
                  alt="faculty_member"
                />
              </div>
            </>
          );
        })}
        <div className="fac-list">
          <ul className="d-flex faculty-social-list">
            <li>
              <div className="icon-back">
                <a href="/">
                  <FaFacebookF />{" "}
                </a>
              </div>
            </li>
            <li>
              <div className="icon-back">
                <a href="/">
                  <AiOutlineInstagram />{" "}
                </a>
              </div>
            </li>
            <li>
              <div className="icon-back">
                <a href="/">
                  <AiOutlineTwitter />{" "}
                </a>
              </div>
            </li>
            <li>
              <div className="icon-back">
                <a href="/">
                  <FaLinkedinIn />{" "}
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};
