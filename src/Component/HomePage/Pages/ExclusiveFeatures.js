import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import axios from "axios";
// import {  useSelector } from "react-redux";
import faculty from "../../../Asserts/Images/faculty.png";
import { apiUrl } from "../../../env";

export const ExclusiveFeatures = () => {
  const dispatch = useDispatch();
  // const featuresList = useSelector(
  //   (state) => state.featureslist.featureslistData
  // );
  // console.log("feature", featuresList);

  const [featuresData, setFeaturesData] = useState([]);

  const getfeatureData = () => {
    axios
      .get(`${apiUrl}/home`)

      .then(function (response) {
        if (response.data) {
          dispatch({
            type: "REQUEST_FEATURESLIST",
            payload: response.data.exclusive_features,
          });
        }
        let ex_feature = response.data.exclusive_features;
        setFeaturesData(ex_feature);
      })

      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    console.log("place");
    getfeatureData();
  }, []);

  return (
    <>
      <div className="exclusive-features" data-aos="zoom-out">
        <div className="container">
          <div className="exclusive">
            <h1 className=" text-center mt-2 fs-3 fw-bold  ">
              Exclusive Features
            </h1>
            <div className="container-fluid">
              <div className="row">
                {[1, 2, 3, 4, 5, 6].map(() => {
                  return (
                    <>
                      {featuresData.map((item) => {
                        // console.log("hello", item);
                        return (
                          <>
                            <div className="col-sm-4">
                              <div className="card border-0">
                                <img
                                  src={apiUrl + "/" + item.image}
                                  alt="faculty"
                                  className="card-img-top"
                                />
                                <div className="card-body">
                                  <h2 className="card-title text-center">
                                    {item.title}
                                  </h2>
                                  <p className="card-text text-center exp">
                                    {item.description}
                                  </p>
                                </div>
                              </div>
                            </div>
                          </>
                        );
                      })}
                    </>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
