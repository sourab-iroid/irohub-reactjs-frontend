import React, { useEffect } from "react";
import Group_2779 from "../../Asserts/bird/Group_2779.png";
import { ExclusiveFeatures } from "./Pages/ExclusiveFeatures";
import { HomeBanner } from "./Pages/HomeBanner";
import { InterviewSchedule } from "./Pages/InterviewSchedule";
import { LiveClasses } from "./Pages/LiveClasses";
import { OfferPage } from "./Pages/OfferPage";
import { OurFaculty } from "./Pages/OurFaculty";
import { PlacementPartners } from "./Pages/PlacementPartners";
import { RegistrationSection } from "./Pages/RegistrationSection";
import { Scholorship } from "./Pages/Scholorship";
import { SuccessStories } from "./Pages/SuccessStories";
import { VideoTestimonials } from "./Pages/VideoTestimonials";
import { WorkTogether } from "./Pages/WorkTogether";
import Aos from "aos";
import "aos/dist/aos.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export const HomePage = () => {
  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);

  return (
    <>
      <div className="first-page">
        <div className="bird">
          <img src={Group_2779} alt="Group_2779" />
        </div>
        <br />

        <HomeBanner />
        <br />
        <OfferPage />
        <br />
        <LiveClasses />
        <br />
        <div>
          <InterviewSchedule />
        </div>
        <br />
        <div>
          <Scholorship />
        </div>
        <br />
        <div>
          <WorkTogether />
        </div>
        <br />
        <div>
          <PlacementPartners />
        </div>
        <br />
        <div>
          <ExclusiveFeatures />
        </div>
        <br />
        <div>
          <VideoTestimonials />
        </div>
        <br />
        <div>
          <OurFaculty />
        </div>
        <br />
        <div>
          <RegistrationSection />
        </div>
        <br />
        <div>
          <SuccessStories />
        </div>
      </div>
    </>
  );
};
