import React, { useState } from "react";

export const FaqDetail = (props) => {
  const [show, setShow] = useState(true);
  return (
    <div>
      {" "}
      <div className="container " data-aos="fade-up">
        {show ? (
          <div
            className="card subcard mt-4  "
            style={{ color: " #0031F2", backgroundColor: "#E9E9E9" }}
          >
            <span onClick={() => setShow(false)} className="ms-5 mt-4 mb-4">
              {props.item.id}.
              <span className=" ms-3">{props.item.faq_question}</span>
            </span>
          </div>
        ) : (
          <div className="card subcard h-100 mt-4">
            <span
              onClick={() => setShow(true)}
              className="ms-5 mt-4 "
              style={{ color: "#0031F2" }}
            >
              {props.item.id}.
              <span className=" ms-3 ">
                <b>{props.item.faq_question}</b>
              </span>
            </span>
            <p className="ps-5 ms-4 mt-3" style={{ color: "black" }}>
              {props.item.faq_answer}
            </p>
          </div>
        )}
      </div>
    </div>
  );
};
