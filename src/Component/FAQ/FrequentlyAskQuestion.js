import React, { useState, useEffect } from "react";
import { FaqDetail } from "./FaqDetail";
import Aos from "aos";
import "aos/dist/aos.css";

export const FrequentlyAskQuestion = () => {
  const faqData = [
    {
      id: 1,
      faq_question: "IROHUB Classes program is available for which classes?",
      faq_answer:
        "Students across classes 4 to 10 can attend and learn from BYJU'S Classes.",
    },
    {
      id: 2,
      faq_question: "IROHUB Classes program is available for which classes?",
      faq_answer:
        "Students across classes 4 to 10 can attend and learn from BYJU'S Classes.",
    },
    {
      id: 3,
      faq_question: "IROHUB Classes program is available for which classes?",
      faq_answer:
        "Students across classes 4 to 10 can attend and learn from BYJU'S Classes.",
    },
    {
      id: 4,
      faq_question: "IROHUB Classes program is available for which classes?",
      faq_answer:
        "Students across classes 4 to 10 can attend and learn from BYJU'S Classes.",
    },
    {
      id: 5,
      faq_question: "IROHUB Classes program is available for which classes?",
      faq_answer:
        "Students across classes 4 to 10 can attend and learn from BYJU'S Classes.",
    },
  ];

  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);

  return (
    <>
      <div className="faqstyle">
        <div className="container-fluid  faqheader " data-aos="fade-up">
          <div className="row" data-aos="zoom-in">
            <div className="text-center mt-5 text-white ">
              <h1>FAQ</h1>
              <p>Q&As to help you understand BYJU'S Classes better</p>
            </div>
          </div>
        </div>
        <div className="container faqcard mt-5">
          <div className="row">
            {faqData.map((item, i) => {
              return <FaqDetail item={item} index={i} key={item.id} />;
            })}
          </div>
        </div>
      </div>
    </>
  );
};
