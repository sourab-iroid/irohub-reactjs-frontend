import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
import MaskGroup from "../../Asserts/Images/Mask Group.png";
function FirstContent() {
  return (
    <div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <ScrollAnimation animateOnce={true} animateIn="fadeIn">
              <img src={MaskGroup} className="maskGroup-img" />
            </ScrollAnimation>
          </div>
          <div class="col-sm-12 col-md-6">
            <div
              className="background-trapezium"
              style={{
                position: "absolute",
                zIndex: "-1",
                width: "50%",
                height: "100%",
                clipPath:
                  "polygon(97.47% -0.15%, 97.59% -0.2%, 27% 100%, 0px 47%, 21.69% 22.04%)",
                background: "#F9F9F9",
              }}
            ></div>
            <h1
              style={{
                fontFamily: "Heebo Bold",
                color: "#5F5F5F",
                fontWeight: "bold",
                opacity: 0.5,
              }}
            >
              Who we are
            </h1>
            <p>
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
              erat, sed diam voluptua. At vero eos et accusam et justo duo
              dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
              sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
              amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
              invidunt ut labore et dolore magna aliquyam erat, sed diam
              voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
              Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum
              dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
              eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
              sed diam voluptua. At vero eos et accusam et justo duo dolores et
              ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
              Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
              sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
              et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
              accusam et justo duo dolores et ea rebum. Stet clita kasd
              gubergren, no sea takimata sanctus est
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FirstContent;
