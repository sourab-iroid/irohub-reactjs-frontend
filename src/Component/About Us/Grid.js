import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
function Grid(props) {
  let userInfo = props.userInfo;
  return (
    <div>
      <p style={{ textAlign: "center", fontSize: "50px" }}>Our Team</p>
      <div class="container">
        <div class="row">
          {userInfo.map((user) => (
            <div class="col-sm-12 col-md-4" style={{ paddingTop: "30px" }}>
              <ScrollAnimation animateIn="fadeIn">
                <img src={user.picture} style={{ width: "80%" }} />
              </ScrollAnimation>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Grid;
