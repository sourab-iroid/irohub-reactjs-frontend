import React from "react";
import { PageHeader } from "../../Common/PageHeader";
import teamMember1 from "../../Asserts/Images/teamMember-1.png";
import teamMember2 from "../../Asserts/Images/teamMember-2.png";
import teamMember3 from "../../Asserts/Images/teamMember-3.png";
import teamMember4 from "../../Asserts/Images/teamMember-4.png";
import teamMember5 from "../../Asserts/Images/teammember-5.png";
import teamMember6 from "../../Asserts/Images/teamMember-6.png";
import video from "../../Asserts/video.png";
import Grid from "./Grid";
import FirstContent from "./FirstContent";
import ScrollAnimation from "react-animate-on-scroll";
function AboutUs() {
  let userInfo = [
    { name: "Anna Brown", position: "Developer", picture: teamMember1 },
    { name: "Anna Brown", position: "Developer", picture: teamMember2 },
    { name: "Anna Brown", position: "Developer", picture: teamMember3 },
    { name: "Anna Brown", position: "Developer", picture: teamMember4 },
    { name: "Anna Brown", position: "Developer", picture: teamMember5 },
    { name: "Anna Brown", position: "Developer", picture: teamMember6 },
  ];
  return (
    <div className="main">
      <PageHeader pageName="ABOUT US" />
      <FirstContent />
      <ScrollAnimation animateOnce={true} animateIn="fadeIn">
        <img src={video} className="video-thumbnail" />
      </ScrollAnimation>

      <Grid userInfo={userInfo} />
    </div>
  );
}

export default AboutUs;
