import React, { useState, useEffect } from "react";
import axios from "axios";
import { PageHeader } from "../Common/PageHeader";
import { ServicesCard } from "./ServiceCards";
import { Header } from "../Common/Header";
import { Footer } from "../Common/Footer";
import { apiUrl } from "../env";

export const ServicesPage = () => {
  const serverURL = "http://13.235.42.102:2000";

  const [servicesData, setServicesData] = useState([]);

  useEffect(() => {
    axios
      .get(`${apiUrl}/services`)
      .then(function (response) {
        //if (response.data.message === "Success")
        response.data.services.map((data) => {
          console.log(apiUrl + "/" + data.image);
        });
        console.log("projectsdata", response.data.services);
        setServicesData(response.data.services);
        //console.log("workflowdata", memberData);
      })
      .catch(function (error) {
        console.error(error);
      });
  }, []);

  return (
    <div className="main">
      {/* <Header /> */}
      {/* <Header /> */}
      <PageHeader pageName="SERVICES" />
      <div className="container-fluid">
        <div
          className="row"
          style={{ paddingLeft: "80px", position: "relative" }}
        >
          {servicesData.map((data) => (
            <div className="col-sm-4">
              <div
                className="card"
                style={{
                  width: "24rem",
                  height: "24rem",
                  marginTop: "10px",
                }}
              >
                <div className="card-body">
                  <img
                    style={{
                      height: "auto",
                      width: "auto",
                      display: "inline",
                    }}
                    src={apiUrl + "/" + data.image}
                  />
                  <h5
                    className="card-title"
                    style={{
                      paddingLeft: "17px",
                      display: "inline",
                      fontFamily: "Akrobat SemiBold",
                    }}
                  >
                    {data.title}
                  </h5>
                  <div className="col-md-12 ">
                    <hr
                      className="section-title-hr"
                      style={{
                        width: "100%",
                        height: "1px",
                        border: "1px solid #5575F2",
                        backgroundColor: "#5575F2",
                        display: "block",
                      }}
                    ></hr>
                  </div>

                  <p className="card-text" style={{ fontFamily: "Segoe UI" }}>
                    {data.description}
                  </p>
                  <div
                    className="col-md-12 "
                    style={{
                      bottom: "40px",
                      paddingRight: "45px",
                      position: "absolute",
                      textAlign: "center",
                    }}
                  >
                    <a href="#" className="btn btn-dark">
                      Learn More
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-arrow-right"
                        viewBox="0 0 16 16"
                        style={{ marginLeft: "5px" }}
                      >
                        <path
                          fill-rule="evenodd"
                          d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"
                        />
                      </svg>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div style={{ padding: "80px" }}></div>

      {/*  */}
      {/* <Footer /> */}
    </div>
  );
};
