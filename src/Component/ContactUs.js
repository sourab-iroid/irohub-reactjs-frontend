import React, { useEffect, useState } from "react";
import contact from "../Asserts/Contactus/contact.png";
import divider from "../Asserts/Contactus/divider.svg";
import location from "../Asserts/Contactus/location.png";
import customer_service from "../Asserts/Contactus/customer_service.png";
import email1 from "../Asserts/Contactus/email.png";
import { AiOutlineDown } from "react-icons/ai";
import Aos from "aos";
import "aos/dist/aos.css";

export const ContactUs = () => {
  const [show, setShow] = React.useState(false);

  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [message, setMessage] = useState("");
  const [fnameErr, setFnameErr] = useState({});
  const [lnameErr, setLnameErr] = useState({});
  const [emailErr, setEmailErr] = useState({});
  const [numberErr, setNumberErr] = useState({});
  const [messageErr, setMessageErr] = useState({});

  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    const isValid = formValidation();
  };

  const formValidation = () => {
    let isValid = true;
    const fnameErr = {};
    const lnameErr = {};
    const emailErr = {};
    const numberErr = {};
    const messageErr = {};

    if (fname.trim() === "") {
      fnameErr.NameEmpty = "please Enter the FirstName";
    }
    if (lname.trim() === "") {
      lnameErr.NameEmpty = "please Enter the LastName";
    }
    if (email.trim() === "") {
      emailErr.emailEmpty = "please Enter the Email";
    }
    if (mobileNumber.trim() === "") {
      numberErr.mobileNumberEmpty = "please Enter the Mobile Number";
    }
    if (message.trim() === "") {
      messageErr.messageEmpty = "please Enter the Message";
    }
    setFnameErr(fnameErr);
    setLnameErr(lnameErr);
    setEmailErr(emailErr);
    setNumberErr(numberErr);
    setMessageErr(messageErr);
  };
  useEffect(() => {
    Aos.init({ duration: 1000 });
  }, []);
  const contactData = [
    {
      id: 1,
      img: location,
      detail1: "2nd floor, Trust Building, Kayyath Lane",
      detail2: "Palarivattom, Cochin – 25",
    },
    {
      id: 2,
      img: customer_service,
      detail1: "+91 81298 55155",
      detail2: "+91 81298 55355",
    },
    {
      id: 2,
      img: email1,
      detail1: "info@iroidtechnologies.com",
      detail2: "info@hub.com",
    },
  ];

  return (
    <>
      <div className="container contactUs-style mb-5">
        <div className="row">
          <div className="col-md-10 mx-auto">
            <div className="row">
              <div className="col-md-7">
                <h1 className="contact-head">Contact Us</h1>
                <p className="contact-head-para">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam voluptua. At vero eos et accusam et
                  justo duo dolores et ea rebum. Stet clita kasd gubergren,
                </p>
                <img src={divider} className="move-border" />
              </div>
              <div className="col-md-5">
                <img src={contact} className="img-fluid contact-img " />
              </div>
            </div>
          </div>
          <div className="contact-divider mt-5">
            <hr />
            <div className="circle">
              <div className="icons">
                <a
                  onClick={() => {
                    setShow(true);
                  }}
                >
                  {" "}
                  <AiOutlineDown className="arrow1 " />
                </a>
              </div>
            </div>
          </div>

          <div className="container">
            <div className="row justify-content-center">
              {show && (
                <div className="col-md-7 ">
                  <h2 className="text-center">How can we help you.</h2>
                  <p className="text-center">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua. At vero eos et accusam et
                    justo duo dolores et ea rebum. Stet clita kasd gubergren,
                  </p>
                  <form onSubmit={onSubmit}>
                    <div className="row contact-form">
                      <div className="col-md-6 ">
                        <label className="">First Name</label> <br />
                        <input
                          type="text"
                          value={fname}
                          onChange={(e) => {
                            setFname(e.target.value);
                          }}
                          className="w-100 contact-form"
                        />
                        {Object.keys(fnameErr).map((key) => {
                          return (
                            <div style={{ color: "red" }}>{fnameErr[key]}</div>
                          );
                        })}
                      </div>
                      <div className="col-md-6">
                        <label>Last Name</label> <br />
                        <input
                          type="name"
                          className="w-100"
                          value={lname}
                          onChange={(e) => {
                            setLname(e.target.value);
                          }}
                        />
                        {Object.keys(lnameErr).map((key) => {
                          return (
                            <div style={{ color: "red" }}>{lnameErr[key]}</div>
                          );
                        })}
                      </div>
                    </div>
                    <div className="contact-form mt-4">
                      <label>Email</label> <br />
                      <input
                        type="email"
                        className="w-100"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                      />
                      {Object.keys(emailErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{emailErr[key]}</div>
                        );
                      })}
                    </div>

                    <div className="contact-form mt-4">
                      <label>Phone Number</label> <br />
                      <input
                        type="number"
                        className="w-100"
                        value={mobileNumber}
                        onChange={(e) => setMobileNumber(e.target.value)}
                      />
                      {Object.keys(numberErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{numberErr[key]}</div>
                        );
                      })}
                    </div>

                    <div className="contact-form mt-4 mb-4">
                      <label>Message</label> <br />
                      <textarea
                        className="w-100"
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                      />
                      {Object.keys(messageErr).map((key) => {
                        return (
                          <div style={{ color: "red" }}>{messageErr[key]}</div>
                        );
                      })}
                    </div>
                    <div className="text-center">
                      <button className="btn btn-outline-primary" type="submit">
                        Submit
                      </button>
                    </div>
                  </form>
                </div>
              )}
            </div>
            <div className=" row mt-3">
              {contactData.map((item) => {
                return (
                  <div className="col-md-4">
                    <div
                      className="card mt-3 ms-3 contact-card "
                      data-aos="zoom-in"
                    >
                      <div className="card-body  text-center mt-3 mb-3">
                        <img src={item.img} className="blink-image" />
                        <div className="text-white mt-3">
                          <p>
                            {item.detail1} <br />
                            {item.detail2}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>

        <div className=" col-md-9 mt-5">
          {/* <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3928.78729717518!2d76.31134021479447!3d10.034403492827623!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfc283d8730e7d0ff!2sIroid%20Technologies!5e0!3m2!1sen!2sin!4v1644837270368!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe> */}
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3928.78729717518!2d76.31134021479447!3d10.034403492827623!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfc283d8730e7d0ff!2sIroid%20Technologies!5e0!3m2!1sen!2sin!4v1644837270368!5m2!1sen!2sin"
            title=" Web Tutorials"
            style={{
              // borderRadius: "20px",
              width: "135%",
              height: "355px",
              frameborder: "0",
            }}
            allowFullScreen
          />
        </div>
      </div>
    </>
  );
};
