import React from "react";
import IrohubLogo from "../Asserts/logo/IrohubLogo.png";
import { AiFillCloseCircle } from "react-icons/ai";
import { FaFacebookF } from "react-icons/fa";
import { AiOutlineInstagram } from "react-icons/ai";
import { AiOutlineTwitter } from "react-icons/ai";
import { FaLinkedinIn } from "react-icons/fa";
import { ForgetPassword } from "./ForgetPassword/ForgetPassword";
import { OTPModal } from "./ForgetPassword/OTPModal";
import { NewPassword } from "./ForgetPassword/NewPassword";
export const DialogSignUp = ({ showSignUp, setShowSignUp }) => {
  const iconStyle = {
    height: "30px",
    width: "30px",
    color: "#002d62",
    right: "25px",
    position: "absolute",
    top: "10px",
    cursor: "pointer",
  };
  return (
    <>
      <div
        className="modal fade signUp-dialogStyle"
        id="signUpModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
        style={{ overflow: "hidden" }}
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="row signUp-relative">
              <div className="col-sm-3 col-md-3 signUp-black-part">
                <div className="signUp-card">
                  <div
                    className="card-body mx-auto"
                    style={{ background: "white" }}
                  >
                    <h2 className="signUp-card-heading">About Irohub</h2>
                    <p className="signUp-card-heading-sub">
                      Lorem ipsum dolor sit amet, consetetur sadipscing elitr,{" "}
                    </p>
                    <p className="card-text signUp-card-sub">
                      {" "}
                      Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                      sed diam nonumy eirmod tempor invidunt ut labore et dolore
                      magna aliquyam erat, sed diam voluptua. At vero eos et
                      accusam et justo duo dolores et ea rebum. Stet clita kasd
                      gubergren, no sea takimata sanctus est Lorem ipsum dolor
                      sit amet.{" "}
                    </p>
                    <div
                      className="signUp-icon-style text-center"
                      style={{ display: "table", marginLeft: "100px" }}
                    >
                      <ul
                        className="signUp-d-inline-flex "
                        style={{ paddingTop: "20px" }}
                      >
                        <li style={{ display: "inline" }}>
                          <a
                            href="#"
                            type="button"
                            className="signUp-social-btns mat-raised-button shadow "
                          >
                            <FaFacebookF className="signUp-icon-style1" />
                          </a>
                        </li>
                        <li style={{ display: "inline" }}>
                          <a
                            href="#"
                            type="button"
                            className="signUp-social-btns mat-raised-button shadow ms-2"
                          >
                            <AiOutlineInstagram className="icon-style1" />
                          </a>
                        </li>
                        <li style={{ display: "inline" }}>
                          <a
                            href="#"
                            type="button"
                            className="signUp-social-btns mat-raised-button shadow ms-2"
                          >
                            <AiOutlineTwitter className="icon-style1" />
                          </a>
                        </li>
                        <li style={{ display: "inline" }}>
                          <a
                            type="button"
                            className="signUp-social-btns mat-raised-button shadow ms-2"
                          >
                            <FaLinkedinIn className="signUp-icon-style1" />
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-9 col-md-9 signUp-main-part">
                <div className="modal-header p-0">
                  <img src={IrohubLogo} className="logo" alt="" />
                  <button
                    type="button"
                    class="btn b-0 mx-3 "
                    data-dismiss="modal"
                  >
                    <AiFillCloseCircle style={iconStyle} id="close-btn" />
                  </button>
                </div>
                <h3 className="signUp-heading  mb-0 fw-bold">
                  Sign Up to Irohub
                </h3>
                <h5 className="signUp-sub-heading ">
                  Enter your details below
                </h5>
                <div className="row justify-content-center signUp-dialog-form">
                  <div className=" col-md-7  ">
                    <form>
                      <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">
                          Name
                        </label>
                        <input
                          type="email"
                          className="form-control"
                          id="exampleInputEmail1"
                          aria-describedby="emailHelp"
                        />
                      </div>
                      <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">
                          Email
                        </label>
                        <input
                          type="email"
                          className="form-control"
                          id="exampleInputEmail1"
                          aria-describedby="emailHelp"
                        />
                      </div>
                      <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">
                          Phone Number
                        </label>
                        <input
                          type="email"
                          className="form-control"
                          id="exampleInputEmail1"
                          aria-describedby="emailHelp"
                        />
                      </div>
                      <div className="mb-3">
                        <label
                          for="exampleInputPassword1"
                          className="form-label"
                        >
                          Password
                        </label>
                        <input
                          type="password"
                          className="form-control"
                          id="exampleInputPassword1"
                        />
                      </div>
                      <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">
                          Confirm Password
                        </label>
                        <input
                          type="email"
                          className="form-control"
                          id="exampleInputEmail1"
                          aria-describedby="emailHelp"
                        />
                      </div>
                      <button
                        type="submit"
                        className="btn  signup-btn mt-3 mb-5"
                      >
                        SIGN UP
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
