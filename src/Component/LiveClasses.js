import React, { useEffect } from "react";
import analytics from "../Asserts/Images/analytics.svg";
import python from "../Asserts/Images/python1.png";
import java from "../Asserts/Images/java1.png";
import { AiOutlineRight } from "react-icons/ai";
import "aos/dist/aos.css";
import Aos from "aos";

export const LiveClasses = (props) => {
  let { name } = props;
  useEffect(() => {
    Aos.init({ duration: 1000 });
  }, []);
  const liveclass = [
    {
      id: 1,
      img: python,
      title: "Lorem ipsum",
      subtitle: "Java",
      describ:
        " Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et doloremagna aliquyam erat, sed diam",
      icon: (
        <AiOutlineRight
          style={{
            fontSize: "24px",
          }}
        />
      ),
    },
    {
      id: 2,
      img: java,
      title: "Lorem ipsum",
      subtitle: "PHP",
      describ:
        " Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et doloremagna aliquyam erat, sed diam",
      icon: (
        <AiOutlineRight
          style={{
            fontSize: "24px",
          }}
        />
      ),
    },
    {
      id: 3,
      img: java,
      title: "Lorem ipsum",
      subtitle: "Web",
      describ:
        " Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et doloremagna aliquyam erat, sed diam",
      icon: (
        <AiOutlineRight
          style={{
            fontSize: "24px",
          }}
        />
      ),
    },
  ];

  const liveclasstwo = [
    {
      id: 1,
      img: python,
      title: "Lorem ipsum",
      subtitle: "Python",
      describ:
        " Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et doloremagna aliquyam erat, sed diam",
      icon: (
        <AiOutlineRight
          style={{
            fontSize: "24px",
          }}
        />
      ),
    },
    {
      id: 2,
      img: java,
      title: "Lorem ipsum",
      subtitle: "Android",
      describ:
        " Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et doloremagna aliquyam erat, sed diam",
      icon: (
        <AiOutlineRight
          style={{
            fontSize: "24px",
          }}
        />
      ),
    },
    {
      id: 3,
      img: java,
      title: "Lorem ipsum",
      subtitle: "IOS",
      describ:
        " Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut labore et doloremagna aliquyam erat, sed diam",
      icon: (
        <AiOutlineRight
          style={{
            fontSize: "24px",
          }}
        />
      ),
    },
  ];

  return (
    <>
      <div className="container placement-liveclass">
        <div className="row">
          <div className="col-md-11 mx-auto">
            <div className="heading mt-5 mb-4 pt-5">
              <div
                className="d-flex justify-content-start"
                data-aos="fade-left"
              >
                <img src={analytics} className="liveimg" />
                <h1 className="heading-text ms-3">{name}</h1>
              </div>
            </div>
            <div className="row" data-aos="fade-right">
              <div className="col-md-6">
                <p className="main-para">
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam voluptua. At vero eos et accusam et
                  justo duo dolores et ea rebum. Stet clita kasd gubergren,
                </p>
                {liveclass.map((item) => {
                  return (
                    <a href="/artBoard">
                      <div
                        className="items d-flex justify-content-between"
                        data-aos="fade-right"
                        onClick={() =>
                          localStorage.setItem(
                            "courseSectionClicked",
                            item.subtitle + " Development"
                          )
                        }
                      >
                        <div className="item-left">
                          <img src={item.img} className="img-part img-fluid" />
                        </div>

                        <div className="item-right">
                          <h3 className="mt-4 heading-first">{item.title}</h3>
                          <h1 className="mt-2 heading-second">
                            {item.subtitle.toUpperCase()} <br /> DEVELOPMENT
                          </h1>
                          <p className="mt-2 mb-4 card-para">{item.describ}</p>

                          <div className="blue-arrows d-inline-block mb-4">
                            {item.icon}
                            {item.icon}
                            {item.icon}
                          </div>
                        </div>
                      </div>
                    </a>
                  );
                })}
              </div>
              <div className="col-md-6" data-aos="fade-left">
                {liveclasstwo.map((item) => {
                  return (
                    <div
                      className="items d-flex justify-content-between"
                      data-aos="fade-left"
                      onClick={() =>
                        localStorage.setItem(
                          "courseSectionClicked",
                          item.subtitle + " Development"
                        )
                      }
                    >
                      <div className="item-left">
                        <img src={item.img} className="img-part img-fluid" />
                      </div>
                      <div className="item-right">
                        <h3 className="mt-4 heading-first">{item.title}</h3>
                        <h1 className="mt-2 heading-second">
                          {item.subtitle} <br /> DEVELOPMENT
                        </h1>
                        <p className="mt-2 mb-4 card-para">{item.describ}</p>
                        <div className="blue-arrows d-inline-block mb-4">
                          {item.icon}
                          {item.icon}
                          {item.icon}
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
