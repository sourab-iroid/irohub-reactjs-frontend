import React from "react";
import IrohubLogo from "../Asserts/logo/IrohubLogo.png";
import { AiFillCloseCircle } from "react-icons/ai";
import { FaFacebookF } from "react-icons/fa";
import { AiOutlineInstagram } from "react-icons/ai";
import { AiOutlineTwitter } from "react-icons/ai";
import { FaLinkedinIn } from "react-icons/fa";
import { ForgetPassword } from "./ForgetPassword/ForgetPassword";
import { OTPModal } from "./ForgetPassword/OTPModal";
import { NewPassword } from "./ForgetPassword/NewPassword";
export const DialogSignIn = (props) => {
  const [modalScreen, setModalScreen] = React.useState(0);

  const handleModalOpen = () => {
    setModalScreen(1);
  };
  const iconStyle = {
    height: "30px",
    width: "30px",
    color: "#002d62",
    right: "25px",
    position: "absolute",
    top: "10px",
    cursor: "pointer",
  };
  return (
    <>
      <div
        class="modal fade dialogStyle"
        id="exampleModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div className="row relative">
              <div className="col-sm-3 col-md-3 black-part">
                <div className="card">
                  <div className="card-body mx-auto">
                    <h2 className="card-heading">About Irohub</h2>
                    <p className="card-heading-sub">
                      Lorem ipsum dolor sit amet, consetetur sadipscing elitr,{" "}
                    </p>
                    <p className="card-text card-sub">
                      {" "}
                      Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                      sed diam nonumy eirmod tempor invidunt ut labore et dolore
                      magna aliquyam erat, sed diam voluptua. At vero eos et
                      accusam et justo duo dolores et ea rebum. Stet clita kasd
                      gubergren, no sea takimata sanctus est Lorem ipsum dolor
                      sit amet.{" "}
                    </p>
                    <div className="icon-style text-center">
                      <ul className="d-inline-flex ">
                        <li>
                          <a
                            href="#"
                            type="button"
                            className="social-btns mat-raised-button shadow "
                          >
                            <FaFacebookF className="icon-style1" />
                          </a>
                        </li>
                        <li>
                          <a
                            href="#"
                            type="button"
                            className="social-btns mat-raised-button shadow ms-2"
                          >
                            <AiOutlineInstagram className="icon-style1" />
                          </a>
                        </li>
                        <li>
                          <a
                            href="#"
                            type="button"
                            className="social-btns mat-raised-button shadow ms-2"
                          >
                            <AiOutlineTwitter className="icon-style1" />
                          </a>
                        </li>
                        <li>
                          <a
                            href="#"
                            type="button"
                            className="social-btns mat-raised-button shadow ms-2"
                          >
                            <FaLinkedinIn className="icon-style1" />
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-9 col-md-9 main-part">
                <div class="modal-header p-0">
                  <img src={IrohubLogo} className="logo" />
                  <button
                    type="button"
                    class="btn b-0 mx-3 "
                    data-dismiss="modal"
                  >
                    <AiFillCloseCircle style={iconStyle} />
                  </button>
                </div>
                <h3 className="heading  mb-0 fw-bold">Sign In to Irohub</h3>
                <h5 className="sub-heading ">Enter your details below</h5>
                <div className="row justify-content-center dialog-form">
                  <div className=" col-md-7  ">
                    <form>
                      <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">
                          Mobile Number
                        </label>
                        <input
                          type="email"
                          class="form-control"
                          id="exampleInputEmail1"
                          aria-describedby="emailHelp"
                        />
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">
                          Password
                        </label>
                        <input
                          type="password"
                          class="form-control"
                          id="exampleInputPassword1"
                        />
                        <a
                          href="#"
                          className="btn forgot-pass"
                          data-toggle="modal"
                          data-target="#forgetModal"
                          onClick={handleModalOpen}
                        >
                          Forget Password ?
                        </a>
                        {/* <ForgetPassword /> */}
                        {modalScreen == 1 && (
                          <ForgetPassword
                            modalScreen={modalScreen}
                            setModalScreen={setModalScreen}
                          />
                        )}
                        {modalScreen == 2 && (
                          <OTPModal
                            modalScreen={modalScreen}
                            setModalScreen={setModalScreen}
                          />
                        )}
                        {modalScreen == 3 && <NewPassword />}
                      </div>

                      <button type="submit" class="btn  signin-btn mt-3 mb-5">
                        SIGN IN
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
