import React from "react";
import { ScholarshipDialog } from "../../ScholarshipDialog";
export const ApplyScholarship = () => {
  let [showApplyNowModal, setShowApplyNowModal] = React.useState(false);
  return (
    <>
      <div className="container-fluid scholarship-apply-banner">
        <div className="row text-white text-center" data-aos="zoom-in">
          <div className="apply-square">
            <h1 className="apply-head">Apply For Scholorship</h1>
            <button
              className="btn apply-button"
              data-toggle="modal"
              data-target="#scholarshipModal"
            >
              Apply Scholarship
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
