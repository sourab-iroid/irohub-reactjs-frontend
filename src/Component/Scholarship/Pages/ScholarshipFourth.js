import React from "react";
import { BsFillPlusCircleFill } from "react-icons/bs";

export const ScholarshipFourth = () => {
  const faqdata = [
    {
      id: 1,
      faq_id: "headingOne",
      faq_colapseid: "#collapseOne",
      faq_colapse: "collapseOne",
      icon: <BsFillPlusCircleFill />,
      faq_question:
        " How to apply for 'Sardar Patel Scholarship for Students Pursuing Graduation'?",
      faq_answer:
        "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,non cupidatat skateboard dolor brunch. Food truck quinoanesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliquaput a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer laborewes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beerfarm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.",
    },
    {
      id: 1,
      faq_id: "headingTwo",
      faq_colapseid: "#collapseTwo",
      faq_colapse: "collapseTwo",
      icon: <BsFillPlusCircleFill />,
      faq_question:
        " How to apply for 'Sardar Patel Scholarship for Students Pursuing Graduation'?",
      faq_answer:
        "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,non cupidatat skateboard dolor brunch. Food truck quinoanesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliquaput a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer laborewes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beerfarm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.",
    },
  ];
  return (
    <>
      <div className="container scholorfourth-style">
        <h1 data-aos="fade-up">Frequently Asked Questions</h1>
        <div className="accordion" id="accordionExample" data-aos="fade-up">
          {faqdata.map((item) => {
            return (
              <div className="card faq-card">
                <div className="card-header" id={item.faq_id}>
                  <h2 className="mb-0">
                    <a
                      className="btn btn-link btn-block text-left "
                      type="button"
                      data-toggle="collapse"
                      data-target={item.faq_colapseid}
                      aria-expanded="true"
                    >
                      {item.icon}
                      <span className="ms-3">{item.faq_question}</span>
                    </a>
                  </h2>
                </div>
                <div
                  id={item.faq_colapse}
                  className="collapse"
                  aria-labelledby={item.faq_id}
                  data-parent="#accordionExample"
                >
                  <div className="card-body">{item.faq_answer}</div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};
