import React from "react";
import people from "../../../Asserts/Images/people.png";
import { BiChevronRightCircle } from "react-icons/bi";

export const ScholarshipThird = () => {
  const eligibledata = [
    {
      id: 1,
      icon: <BiChevronRightCircle className="ssicon-style" />,
      eli_data:
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sediam nonumy eirmod tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua. At vero eos et accusam e",
    },
    {
      id: 2,
      icon: <BiChevronRightCircle className="ssicon-style" />,
      eli_data:
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sediam nonumy eirmod tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua. At vero eos et accusam e",
    },
    {
      id: 3,
      icon: <BiChevronRightCircle className="ssicon-style" />,
      eli_data:
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sediam nonumy eirmod tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua. At vero eos et accusam e",
    },
    {
      id: 4,
      icon: <BiChevronRightCircle className="ssicon-style" />,
      eli_data:
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sediam nonumy eirmod tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua. At vero eos et accusam e",
    },
    {
      id: 5,
      icon: <BiChevronRightCircle className="ssicon-style" />,
      eli_data:
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sediam nonumy eirmod tempor invidunt ut labore et dolore magnaaliquyam erat, sed diam voluptua. At vero eos et accusam e",
    },
  ];
  return (
    <>
      <div className="container-fluid ssthird mt-5">
        <div className="row">
          <div className="col-md-4 ssthird " data-aos="fade-right">
            <img src={people} className="img-fluid ss-image" />
          </div>
          <div className="col-md-8 ssthird" data-aos="fade-left">
            <div className="scholorship-square text-white">
              <h3 className="sub-head">We believe that our works can</h3>
              <h1 className="head">Eligibility</h1>
              <div className="details">
                {/* <div className="d-flex">
                  <BiChevronRightCircle className="ssicon-style" />
                  <p className="detail-para">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua. At vero eos et accusam e{" "}
                  </p>
                </div> */}
                {eligibledata.map((item) => {
                  return (
                    <div className="d-flex mt-1">
                      {/* <BiChevronRightCircle className="ssicon-style" /> */}
                      {item.icon}
                      <p className="detail-para">{item.eli_data}</p>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
