import React from "react";
import { AiOutlineMail } from "react-icons/ai";

export const ScholarshipContact = () => {
  return (
    <>
      <div className="container " data-aos="fade-up">
        <div className="contactscholar-style text-center">
          <h1 className="contact-head">
            Contact <span className="us"> Us</span>
          </h1>
          <hr className="line " />
          <p className="contact-para mb-0">
            In case of queries.please reach out to us
          </p>
        </div>
        <div className="d-flex justify-content-center">
          <AiOutlineMail className="contact-icon" />
          <p className="contact-para p-0"> info@irohub.com</p>
        </div>
      </div>
    </>
  );
};
