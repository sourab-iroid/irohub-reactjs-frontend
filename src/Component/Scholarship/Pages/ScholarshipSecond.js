import React from "react";
import scholar_graduation from "../../../Asserts/Images/scholar_graduation.png";
import girl_typing_laptop from "../../../Asserts/Images/girl_typing_laptop.png";

export const ScholarshipSecond = () => {
  return (
    <>
      <div className="container mt-5" data-aos="fade-up">
        <div className="row">
          <div className=" col-md-6" data-aos="fade-right">
            <div >
              {/* <div className="ssbottom-shape"></div> */}
              <img src={scholar_graduation} className="img-fluid ssimage" />
            </div>

            <div>
              {/* <div className="ssbottom-shape1"></div> */}
              <img src={girl_typing_laptop} className="img-fluid ssimage1" />
            </div>
          </div>
          <div className=" col-md-6">
            <div className="about-Scholorship">
              <h1 className="scholorship-head">Scholorship</h1>
              <p className="scholorship-para">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea
                takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
                dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
                eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
                sed diam voluptua. At vero eos et accusam et justo duo dolores
                et ea rebum. SteLorem ipsum dolor sit amet, consetetur
                sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                labore et dolore magna aliquyam erat, sed diam voluptua. At vero
                eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
                gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
                amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
                sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum. Stet clita kasd gubergren,{" "}
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
