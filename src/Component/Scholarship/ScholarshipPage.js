import React, { useEffect } from "react";
import { VideoBannerFive } from "../HomePage/Pages/VideoBannerFive";
import { ApplyScholarship } from "./Pages/ApplyScholarship";
import { ScholarshipContact } from "./Pages/ScholarshipContact";
import { ScholarshipFourth } from "./Pages/ScholarshipFourth";
import { ScholarshipSecond } from "./Pages/ScholarshipSecond";
import { ScholarshipThird } from "./Pages/ScholarshipThird";
import Aos from "aos";
import "aos/dist/aos.css";

export const ScholarshipPage = () => {
  useEffect(() => {
    Aos.init({ duration: 2000 });
  }, []);
  return (
    <>
      <div className="first-page">
        <VideoBannerFive label="Apply Now" />
      </div>
      <div className="scholar-second">
        <ScholarshipSecond />
      </div>
      <div className="scholar-third">
        <ScholarshipThird />
      </div>
      <div className="scholar-fourth mt-5">
        <ScholarshipFourth />
      </div>
      <div className="scholar-apply mt-5">
        <ApplyScholarship />
      </div>
      <div className="scholar-contact mt-5">
        <ScholarshipContact />
      </div>
    </>
  );
};
