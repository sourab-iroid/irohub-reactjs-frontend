import React from "react";

function CategoryBox() {
  return (
    <div
      className="category-box mb-5 wow animate__ animate__backInUp animate__slower  animated"
      style={{ visibility: "visible" }}
    >
      <h3>Categories</h3>
      <div className="category-item d-flex justify-content-between">
        <span>Android Development</span>
        <span className="category-count">9</span>
      </div>
      <div className="category-item-none d-flex justify-content-between">
        <span>Apps</span>
        <span className="category-count">8</span>
      </div>
      <div className="category-item d-flex justify-content-between">
        <span>Internship</span>
        <span className="category-count">7</span>
      </div>
      <div className="category-item-none d-flex justify-content-between">
        <span>IOS</span>
        <span className="category-count">6</span>
      </div>
      <div className="category-item d-flex justify-content-between">
        <span>Java</span>
        <span className="category-count">5</span>
      </div>
      <div className="category-item-none d-flex justify-content-between">
        <span>PHP</span>
        <span className="category-count">4</span>
      </div>
      <div className="category-item d-flex justify-content-between">
        <span>Python</span>
        <span className="category-count">3</span>
      </div>
    </div>
  );
}

export default CategoryBox;
