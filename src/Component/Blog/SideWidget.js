import React from "react";
import Group_2807 from "../../Asserts/Images/Group_2807.png";
function SideWidget() {
  return (
    <div
      className="widgets mb-5 wow animate__ animate__backInUp animate__slower  animated"
      // style="visibility: visible;"
      style={{ visibility: "visible" }}
    >
      <h3>Latest post</h3>
      <div>
        <a href="#" className="text-decoration-none">
          <div className="widget-item d-flex align-items-center mb-3">
            <div className="widget-img">
              <img alt="img-widget" className="img-fluid" src={Group_2807} />
            </div>
            <div className="title">
              <strong>
                Why your eCommerce stores need a mobile application?
              </strong>
              <div className="mt-2 date"> January 02 2020 </div>
            </div>
          </div>
        </a>
      </div>
      <div>
        <a href="#" className="text-decoration-none">
          <div className="widget-item d-flex align-items-center mb-3">
            <div className="widget-img">
              {/* <img alt="img-widget" className="img-fluid" src="../../../assets/image/corporate1.png"> */}
            </div>
            <div className="title">
              <strong>
                Why your eCommerce stores need a mobile application?
              </strong>
              <div className="mt-2 date"> December 05 2020 </div>
            </div>
          </div>
        </a>
      </div>
      <div>
        <a href="#" className="text-decoration-none">
          <div className="widget-item d-flex align-items-center mb-3">
            <div className="widget-img">
              <img alt="img-widget" className="img-fluid" src={Group_2807} />
            </div>
            <div className="title">
              <strong>
                Why your eCommerce stores need a mobile application?
              </strong>
              <div className="mt-2 date"> December 02 2020 </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  );
}

export default SideWidget;
