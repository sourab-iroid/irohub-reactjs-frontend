import React from "react";
import CategoryBox from "./CategoryBox";
import Grid from "./Grid";
import SideWidget from "./SideWidget";
import ScrollAnimation from "react-animate-on-scroll";
function Blog() {
  return (
    <div>
      <ScrollAnimation animateIn="fadeIn">
        <h1
          className="blog-heading mt-5 mb-5 wow animate__ animate__fadeIn animate__slower  animated"
          style={{ visibility: "visible", padding: "15px" }}
        >
          Blog
        </h1>
      </ScrollAnimation>

      <div className="row" style={{ padding: "15px" }}>
        <div className="col-md-8">
          <div className="row">
            <Grid />
            <Grid />
            <Grid />
            <Grid />
            <Grid />
            <Grid />
            <Grid />
            <Grid />
          </div>
        </div>
        <div className="col-md-4">
          <ScrollAnimation animateIn="fadeInUp">
            <SideWidget />
          </ScrollAnimation>
          <ScrollAnimation animateIn="fadeInUp">
            <CategoryBox />
          </ScrollAnimation>
        </div>
      </div>
    </div>
  );
}

export default Blog;
