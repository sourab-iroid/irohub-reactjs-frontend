import React from "react";
import Group_2805 from "../../Asserts/Images/Group_2805.png";
import ScrollAnimation from "react-animate-on-scroll";
function Grid() {
  return (
    <div
      className="col-sm-12 col-md-6 wow animate__ animate__fadeInUp animate__slower  animated"
      //   style="visibility: visible;"
      style={{ visibility: "visible" }}
    >
      <ScrollAnimation animateIn="fadeInUp">
        <div className="mb-5">
          <a
            href="/home-blog-details"
            routerlink="/home-blog-details"
            className="text-decoration-none"
          >
            <div className="card">
              <div className="card-body img-card">
                <img alt="blog-img" className="img-fluid" src={Group_2805} />
              </div>
            </div>
          </a>
        </div>
      </ScrollAnimation>
    </div>
  );
}

export default Grid;
