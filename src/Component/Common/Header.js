import React from "react";
import IrohubLogo from "../../Asserts/logo/IrohubLogo.png";

export const Header = () => {
  return (
    <>
      <header className="header-style">
        <div className="container">
          <nav className="row navbar navbar-expand-lg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ">
            <div className="container-fluid">
              <a className="navbar-brand" href="/">
                <img
                  src={IrohubLogo}
                  alt="Irohublogo"
                  className="image-fluid brand-logo"
                />
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div
                className="collapse navbar-collapse mb-3 justify-content-end"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav  ">
                  <li className="nav-item">
                    <a
                      className="nav-link active navigations"
                      aria-current="page"
                      href="/"
                    >
                      Live Classes
                      {/* <Link to="">Live Classes</Link> */}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link navigations" href="/">
                      Video Classes
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link navigations" href="/">
                      Testimonials
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link navigations" href="/">
                      Our Services
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link navigations" href="/">
                      Get In Touch
                    </a>
                  </li>
                </ul>
              </div>
              <form className="d-flex mb-3">
                <button
                  className="btn btn-outline-primary button-sign"
                  type="submit"
                >
                  Sign In
                </button>
                <button
                  className="btn btn-outline-primary button-sign ms-3"
                  type="submit"
                >
                  Sign up
                </button>
              </form>
            </div>
          </nav>
        </div>
      </header>
    </>
  );
};
