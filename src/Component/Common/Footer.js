import React from "react";
import IrohubLogo from "../../Asserts/logo/IrohubLogo.png";

export const Footer = () => {
  return (
    <>
      <footer className="footer-style">
        <div className="shapes-footer">
          <div className="black-shape"></div>
          <div className="blue-right"></div>
          <div className="blue-left"></div>
        </div>
        <div className="black-bg">
          <div className="container">
            <div className=" row wow">
              <div className="col-md-4">
                <img src={IrohubLogo} className="foot-logo" />
                <p className="footer-para">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Laborum obcaecati dignissimos quae quo ad iste ipsum officiis
                  deleniti asperioressit.
                </p>
              </div>
              <div className="col-md-5 ">
                <h3 className="small-headings mt-3">LINKS</h3>
                <hr className="hr-small" />
                <div className="row foot-link">
                  <div className="col-md-5 text-white">
                    <ul id="list">
                      <li>
                        <span className="ms-3 ">about us</span>
                      </li>
                      <li>
                        <span className="ms-3">Blog</span>
                      </li>
                      <li>
                        <span className="ms-3">faq</span>
                      </li>
                      <li>
                        <span className="ms-3">contact us</span>
                      </li>
                    </ul>
                  </div>
                  <div className="col-md-7 p-0 text-white">
                    <ul id="list">
                      <li>
                        <span className="ms-3">Help & Support</span>{" "}
                      </li>
                      <li>
                        <span className="ms-3">Terms & Contitions</span>
                      </li>
                      <li>
                        <span className="ms-3">privacy policy</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-md-3 text-white">
                <h3 className="small-headings mt-3">SERVICES</h3>
                <hr className="hr-small" />
                <ul id="list">
                  <li>
                    <span className="ms-3">project ideas</span>
                  </li>
                  <li>
                    <span className="ms-3">live training</span>
                  </li>
                  <li>
                    <span className="ms-3">video training</span>
                  </li>
                </ul>
              </div>
            </div>
            <hr className="mt-4 mb-4" />
            <div className="row pb-5">
              <div className="col-md-4">
                <h3 className="small-headings mt-3">OFFICE ADDRESS</h3>
                <hr className="hr-small" />
                <p className="footer-para">
                  1st Floor, Trust building, Kayyath Ln, <br /> Palarivattom,
                  Kochi{" "}
                </p>
              </div>
              <div className="col-md-5">
                <h3 className="small-headings mt-3">QUICK CONTACTS</h3>
                <hr className="hr-small" />
                <p className="footer-para">
                  info@Irohub.com <br /> +91 8129855155{" "}
                </p>
              </div>
              <div className="col-md-3">
                <h3 className="small-headings mt-3">CONNECTED</h3>
                <hr className="hr-small" />

                <ul className="Socialicon list-unstyled">
                  <li>
                    <img
                      src="https://www.cookiemanindia.com/assets/images/svg/twitter_icon.svg"
                      class="img-fluid "
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://www.cookiemanindia.com/assets/images/svg/facebook_icon.svg"
                      class="img-fluid "
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://www.cookiemanindia.com/assets/images/svg/instagram_icon.svg"
                      class="img-fluid "
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://www.cookiemanindia.com/assets/images/svg/youtube_icon.svg"
                      class="img-fluid "
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://www.cookiemanindia.com/assets/images/svg/linkedin_icon.svg"
                      class="img-fluid "
                      alt=""
                    />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="blue-bottom">
            <p className="rights-bottom ">
              © IROHUB. ALL Rights Reserved. Powered by Iroid Technologies
            </p>
          </div>
        </div>
      </footer>
    </>
  );
};
