import React from "react";
import TagElements from "./TagElements";

function Tags() {
  let tagContents = ["ANDROID APP DEVELOPMENT", , "APP"];
  let [tagElements, setTagElements] = React.useState();
  React.useEffect(() => {
    setTagElements(tagContents);
  }, []);
  return (
    <div
      className="category-box mb-5 wow animate__ animate__backInUp animate__slower  animated"
      style={{ visibility: "visible" }}
    >
      <h3 style={{ fontSize: "initial" }}>Tags</h3>
      <TagElements tagElements={tagElements} />
    </div>
  );
}

export default Tags;
