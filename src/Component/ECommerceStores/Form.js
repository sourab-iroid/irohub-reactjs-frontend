import React from "react";

function Form() {
  const [commentsCount, setCommentsCount] = React.useState(0);
  return (
    <div className="form" style={{ paddingLeft: "70px" }}>
      <h2 style={{ fontSize: "x-large" }}>Post Comments ({commentsCount})</h2>
      <h2
        style={{
          fontSize: "x-large",
          marginTop: "70px",
          paddingLeft: "40px",
        }}
      >
        Leave a reply
      </h2>
      <form>
        {/*  */}
        <div class="row" style={{ padding: "35px" }}>
          <div class="col">
            <input
              width="50%"
              placeholder="Name"
              type="text"
              id="border-bottom-input"
              class="form-control"
              // placeholder="First name"
            />
          </div>
          <div class="col">
            <input
              width="50%"
              placeholder="Email"
              type="text"
              id="border-bottom-input"
              class="form-control"
              // placeholder="Last name"
            />
          </div>

          <div style={{ marginTop: "30px" }}>
            <div class="custom-file" style={{ marginTop: "15px" }}>
              <input
                type="text"
                placeholder="Comment"
                id="border-bottom-input"
                class="form-control"
                // placeholder="Last name"
              />
            </div>
          </div>
        </div>
        <button
          type="button"
          class="btn btn-outline-secondary"
          style={{
            position: "relative",
            left: "35px",
            borderRadius: "16px",
            width: "170px",
            marginBottom: "15px",
          }}
        >
          SUBMIT
        </button>
      </form>
    </div>
  );
}

export default Form;
