import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
import SideWidget from "./SideWidget";
import CategoryBox from "./CategoryBox";
import LeftSideSection from "./LeftSideSection";
import Form from "./Form";
import Tags from "./Tags";
function ECommerceStores() {
  return (
    <div>
      <ScrollAnimation animateIn="fadeIn"></ScrollAnimation>

      <div className="row" style={{ padding: "15px" }}>
        <div className="col-md-8" style={{ paddingLeft: "70px" }}>
          <LeftSideSection />
        </div>
        <div className="col-md-4">
          <ScrollAnimation animateIn="fadeInUp">
            <SideWidget />
          </ScrollAnimation>
          <ScrollAnimation animateIn="fadeInUp">
            <CategoryBox />
          </ScrollAnimation>
          <ScrollAnimation animateIn="fadeInUp">
            <Tags />
          </ScrollAnimation>
        </div>
      </div>
      <Form />
    </div>
  );
}

export default ECommerceStores;
