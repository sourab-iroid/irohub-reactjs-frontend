import React from "react";

import eCommerceStores from "../../Asserts/Images/eCommerceStores.png";
function LeftSideSection() {
  return (
    <div>
      <div className="row">
        <img src={eCommerceStores} style={{ width: "100%" }} />
        <div className="title">
          <strong style={{ fontSize: "xx-large" }}>
            Why your eCommerce stores need a mobile application?
          </strong>
          <div className="mt-2 date" style={{ opacity: 0.5 }}>
            IROHUB INFOTECH | January 02 2020{" "}
          </div>
        </div>
        <div className="content" style={{ textAlign: "justify" }}>
          <p>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
            nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
            erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
            et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
            Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur
            sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
            et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
            accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
            no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
            dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
            tempor invidunt
          </p>
        </div>
      </div>
    </div>
  );
}

export default LeftSideSection;
