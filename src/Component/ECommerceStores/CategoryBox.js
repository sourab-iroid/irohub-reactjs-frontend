import React from "react";

function CategoryBox() {
  return (
    <div
      className="category-box mb-5 wow animate__ animate__backInUp animate__slower  animated"
      style={{ visibility: "visible" }}
    >
      <h3 style={{ fontSize: "initial" }}>Categories</h3>
      <div className="category-item d-flex justify-content-between">
        <span>
          <p style={{ fontSize: "small" }}>Android Development</p>
        </span>
        <span className="category-count">9</span>
      </div>
      <div className="category-item-none d-flex justify-content-between">
        <span>
          <p style={{ fontSize: "small" }}>Apps</p>
        </span>
        <span className="category-count">8</span>
      </div>
      <div className="category-item d-flex justify-content-between">
        <span>
          <p style={{ fontSize: "small" }}>Internship</p>
        </span>
        <span className="category-count">7</span>
      </div>
      <div className="category-item-none d-flex justify-content-between">
        <span>
          <p style={{ fontSize: "small" }}>IOS</p>
        </span>
        <span className="category-count">6</span>
      </div>
      <div className="category-item d-flex justify-content-between">
        <span>
          <p style={{ fontSize: "small" }}>Java</p>
        </span>
        <span className="category-count">5</span>
      </div>
      <div className="category-item-none d-flex justify-content-between">
        <span>
          <p style={{ fontSize: "small" }}>PHP</p>
        </span>
        <span className="category-count">4</span>
      </div>
      <div className="category-item d-flex justify-content-between">
        <span>
          <p style={{ fontSize: "small" }}>Python</p>
        </span>
        <span className="category-count">3</span>
      </div>
    </div>
  );
}

export default CategoryBox;
