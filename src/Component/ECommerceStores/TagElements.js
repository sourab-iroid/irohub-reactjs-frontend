import React from "react";

function TagElements(props) {
  let tagElements = props.tagElements;
  return (
    <div>
      {tagElements &&
        tagElements.map((el) => {
          return (
            <div
              className="Tag"
              style={{
                padding: "5px",
                border: "1px solid #eee",
                textAlign: "center",
                borderRadius: "20px",
                width: "max-content",
              }}
            >
              <p>#{el}</p>
            </div>
          );
        })}
    </div>
  );
}

export default TagElements;
