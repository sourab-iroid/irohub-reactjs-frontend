import React from "react";
import Group_2807 from "../../Asserts/Images/Group_2807.png";
function SideWidget() {
  return (
    <div
      className="widgets mb-5 wow animate__ animate__backInUp animate__slower  animated"
      // style="visibility: visible;"
      style={{ visibility: "visible" }}
    >
      <h3 style={{ fontSize: "initial" }}>Latest Project</h3>
      <div>
        <a
          href="#"
          className="text-decoration-none"
          style={{ color: "#4D4D4D" }}
        >
          <div className="widget-item d-flex align-items-center mb-3">
            <div className="widget-img">
              <img alt="img-widget" className="img-fluid" src={Group_2807} />
            </div>
            <div className="title">
              <strong>
                Why your eCommerce stores need a mobile application?
              </strong>
              <div className="mt-2 date">
                {" "}
                <p style={{ color: "#AAAAAA" }}>January 02 2020</p>{" "}
              </div>
            </div>
          </div>
        </a>
      </div>
      <div>
        <a
          href="#"
          className="text-decoration-none"
          style={{ color: "#4D4D4D" }}
        >
          <div className="widget-item d-flex align-items-center mb-3">
            <div className="widget-img">
              <img alt="img-widget" className="img-fluid" src={Group_2807} />
            </div>
            <div className="title">
              <strong>
                Why your eCommerce stores need a mobile application?
              </strong>
              <div className="mt-2 date">
                {" "}
                <p style={{ color: "#AAAAAA" }}>January 02 2020</p>{" "}
              </div>
            </div>
          </div>
        </a>
      </div>
      <div>
        <a
          href="#"
          className="text-decoration-none"
          style={{ color: "#4D4D4D" }}
        >
          <div className="widget-item d-flex align-items-center mb-3">
            <div className="widget-img">
              <img alt="img-widget" className="img-fluid" src={Group_2807} />
            </div>
            <div className="title">
              <strong>
                Why your eCommerce stores need a mobile application?
              </strong>
              <div className="mt-2 date">
                <p style={{ color: "#AAAAAA" }}>December 02 2020</p>{" "}
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  );
}

export default SideWidget;
