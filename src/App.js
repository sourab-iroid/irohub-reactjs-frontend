import "./App.css";
import { Header } from "./Common/Header";
import { Footer } from "./Common/Footer";
import { HomePage } from "./Component/HomePage/HomePage";
import { FrequentlyAskQuestion } from "./Component/FAQ/FrequentlyAskQuestion";
import "./Component/scss/style.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { PlacementPage } from "./Component/PlacementPage";
import { ServicesPage } from "./Component/ServicesPage";
import { LiveClasses } from "./Component/LiveClasses";
import { VideoClasses } from "./Component/VideoClasses";
import AboutUs from "./Component/About Us/AboutUs";
import ProjectIdeas from "./ProjectIdeas/ProjectIdeas";
import Blog from "./Component/Blog/Blog";
import { ContactUs } from "./Component/ContactUs";
import ECommerceStores from "./Component/ECommerceStores/ECommerceStores";
import ArtBoard from "./Component/ArtBoard/ArtBoard";
import { PrivacyPolicy } from "./Component/PrivacyPolicy";
import { TermsAndConditions } from "./Component/TermsAndConditions";
import { ScholarshipDialog } from "./Component/ScholarshipDialog";
import { ScholarshipPage } from "./Component/Scholarship/ScholarshipPage";
import { ForgetPassword } from "./Component/ForgetPassword/ForgetPassword";
export default function App() {
  return (
    <>
      <Header />
      <ScholarshipDialog />
      {/* <ForgetPassword /> */}
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/placement" element={<PlacementPage />} />
          <Route path="/services" element={<ServicesPage />} />
          <Route
            path="/liveClasses"
            element={<LiveClasses name="LIVE CLASSES" />}
          />
          <Route path="/videoClasses" element={<VideoClasses />} />
          {/* <Route path="/videoClasses" element={<LiveClasses name="VIDEO CLASSES"/>} /> */}
          <Route path="/aboutUs" element={<AboutUs />} />
          <Route path="/projectIdeas" element={<ProjectIdeas />} />
          <Route path="/blog" element={<Blog />} />
          <Route path="/contact_us" element={<ContactUs />} />
          <Route path="/faq" element={<FrequentlyAskQuestion />} />
          <Route path="/eCommerceStores" element={<ECommerceStores />} />
          <Route path="/artBoard" element={<ArtBoard />} />
          <Route path="/termsAndConditions" element={<TermsAndConditions />} />
          <Route path="/privacy_policy" element={<PrivacyPolicy />} />
          <Route path="/scholarship" element={<ScholarshipPage />} />
        </Routes>
      </BrowserRouter>
      <Footer />
    </>
  );
}
