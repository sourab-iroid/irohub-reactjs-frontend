import { USER } from "../Constants/userprofileConstants";
import { RECORDS } from "../Constants/featureConstants";
import userprofileservice from "../services/userprofileservice";
import vitalService from "../services/vitalService";

export const getStudentlist = (data) => async (dispatch) => {
  request();
  const res = await getStudentlist(data);
  try {
    success(res);
    return Promise.resolve(res.data);
  } catch (err) {
    failure(err);
    return Promise.reject(err);
  }

  function request() {
    dispatch({ type: RECORDS.REQUEST_FEATURESLIST });
  }
};
