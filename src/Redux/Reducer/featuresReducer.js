import { RECORDS } from "../Constants/featureConstants";

const initialState = {
  featureslistData: [
    // {
    //   id: 1,
    //   img_path: "",
    //   title: "Exceptional Faculty",
    //   desc: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod Lorem ipsum dolor sit amet, consetetu r sadipscing elitr,",
    // },
    // {
    //   id: 2,
    //   img_path: "",
    //   title: "Exceptional Faculty",
    //   desc: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod Lorem ipsum dolor sit amet, consetetu r sadipscing elitr,",
    // },
    // {
    //   id: 3,
    //   img_path: "",
    //   title: "Exceptional Faculty",
    //   desc: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod Lorem ipsum dolor sit amet, consetetu r sadipscing elitr,",
    // },
    // {
    //   id: 4,
    //   img_path: "",
    //   title: "Exceptional Faculty",
    //   desc: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod Lorem ipsum dolor sit amet, consetetu r sadipscing elitr,",
    // },
    // {
    //   id: 5,
    //   img_path: "",
    //   title: "Exceptional Faculty",
    //   desc: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod Lorem ipsum dolor sit amet, consetetu r sadipscing elitr,",
    // },
    // {
    //   id: 6,
    //   img_path: "",
    //   title: "Exceptional Faculty",
    //   desc: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod Lorem ipsum dolor sit amet, consetetu r sadipscing elitr,",
    // },
  ],
};

const featuresReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case RECORDS.REQUEST_FEATURESLIST:
      return {
        ...state,
        featureslistData: payload,
      };

    default:
      return state;
  }
};

export default featuresReducer;
