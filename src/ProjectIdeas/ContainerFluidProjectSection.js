import React from "react";
import ScrollAnimation from "react-animate-on-scroll";

import img5 from "../Asserts/Images/img-05.png";
function ContainerFluidProjectSection() {
  return (
    <div className="container-fluid project-section">
      <div className="row">
        <div className="col-md-5 p-0">
          <ScrollAnimation animateIn="fadeInUp">
            <img
              src={img5}
              className="img-fluid kp-img wow animate__ animate__fadeInUpBig animate__slow   animated"
              //   style="visibility: visible; animation-name: fadeInUpBig;"
              style={{
                visibility: "visible",
                animationName: "fadeInUpBig",
                height: "750px",
              }}
            />
          </ScrollAnimation>
        </div>
        <div className="col-md-7 p-0">
          <ScrollAnimation animateIn="fadeInUp">
            <div
              className="project-square wow animate__ animate__fadeInUpBig animate__slow   animated"
              style={{ visibility: "visible", animationName: "fadeInUpBig" }}
            >
              <ScrollAnimation animateIn="fadeIn">
                <h3
                  className="project-subhead wow animate__ animate__fadeIn animate__slower"
                  style={{ visibility: "visible" }}
                >
                  We believe that our works can
                </h3>
              </ScrollAnimation>
              <ScrollAnimation animateIn="fadeIn">
                <h1
                  className="project-head wow animate__ animate__fadeIn animate__slower"
                  // style="visibility: visible; animation-name: fadeIn;"
                  style={{ visibility: "visible", animationName: "fadeIn" }}
                >
                  What Kind Of Project ?{" "}
                </h1>
              </ScrollAnimation>

              <div id="accordionExample" className="accordion">
                <ScrollAnimation animateIn="fadeIn">
                  <div
                    className="card mb-5 wow animate__ animate__fadeIn animate__slower"
                    style={{ visibility: "visible" }}
                  >
                    <div className="xa">
                      <div
                        id="headingOne"
                        className="card-header"
                        style={{
                          backgroundColor: "#041584",
                          padding: "0.75rem 1.25rem",
                          marginBottom: 0,
                        }}
                      >
                        <h2 className="mb-0">
                          <a
                            href="#"
                            type="button"
                            data-toggle="collapse"
                            data-target="#collapseOne"
                            aria-expanded="true"
                            aria-controls="collapseOne"
                            className="d-flex justify-content-between buttons"
                            style={{ textDecoration: "none" }}
                          >
                            <span
                              style={{
                                color: "#fff",
                                font: "500 20px/32px Roboto,Helvetica Neue,sans-serif",
                              }}
                            >
                              Let's Make Something Creative
                            </span>
                            <p style={{ color: "white" }}>+</p>
                          </a>
                        </h2>
                      </div>
                    </div>
                    <div
                      id="collapseOne"
                      aria-labelledby="headingOne"
                      data-parent="#accordionExample"
                      className="collapse"
                    >
                      <div className="card-body">
                        {" "}
                        Anim pariatur cliche reprehenderit, enim eiusmod high
                        life accusamus terry richardson ad squid. 3 wolf moon
                        officia aute, non cupidatat skateboard dolor brunch.
                        Food truck quinoa nesciunt laborum eiusmod. 3 wolf moon
                        tempor, sunt aliqua put a bird on it squid single-origin
                        coffee nulla shoreditch et. Nihil anim keffiyeh
                        helvetica, craft beer labore wes anderson cred sapiente
                        ea proident. Ad vegan excepteur butcher vice lomo.
                        Leggings occaecat craft farm-to-table, raw denim
                        aesthetic synth nesciunt you probably haven't heard of
                        them labore sustainable VHS.{" "}
                      </div>
                    </div>
                  </div>
                </ScrollAnimation>
                <ScrollAnimation animateIn="fadeIn">
                  <div
                    className="card mb-5 wow animate__ animate__fadeIn animate__slower"
                    style={{ visibility: "visible", animationName: "fadeIn" }}
                  >
                    <div className="xa">
                      <div
                        id="headingTwo"
                        className="card-header"
                        style={{
                          backgroundColor: "#041584",
                          padding: "0.75rem 1.25rem",
                          marginBottom: 0,
                        }}
                      >
                        <h2 className="mb-0">
                          <a
                            href="#"
                            type="button"
                            data-toggle="collapse"
                            data-target="#collapseTwo"
                            aria-expanded="false"
                            aria-controls="collapseTwo"
                            className="d-flex justify-content-between buttons collapsed"
                            style={{ textDecoration: "none" }}
                          >
                            <span
                              style={{
                                color: "#fff",
                                font: "500 20px/32px Roboto,Helvetica Neue,sans-serif",
                              }}
                            >
                              Let's Make Something Creative
                            </span>
                            <p style={{ color: "white" }}>+</p>
                          </a>
                        </h2>
                      </div>
                    </div>
                    <div
                      id="collapseTwo"
                      aria-labelledby="headingTwo"
                      data-parent="#accordionExample"
                      className="collapse"
                    >
                      <div className="card-body">
                        {" "}
                        Anim pariatur cliche reprehenderit, enim eiusmod high
                        life accusamus terry richardson ad squid. 3 wolf moon
                        officia aute, non cupidatat skateboard dolor brunch.
                        Food truck quinoa nesciunt laborum eiusmod. 3 wolf moon
                        tempor, sunt aliqua put a bird on it squid single-origin
                        coffee nulla shoreditch et. Nihil anim keffiyeh
                        helvetica, craft beer labore wes anderson cred sapiente
                        ea proident. Ad vegan excepteur butcher vice lomo.
                        Leggings occaecat craft farm-to-table, raw denim
                        aesthetic synth nesciunt you probably haven't heard of
                        them labore sustainable VHS.{" "}
                      </div>
                    </div>
                  </div>
                </ScrollAnimation>
                <ScrollAnimation animateIn="fadeIn">
                  <div
                    className="card mb-5 wow animate__ animate__fadeIn animate__slower"
                    style={{ visibility: "visible", animationName: "fadeIn" }}
                  >
                    <div className="xa">
                      <div
                        id="headingThree"
                        className="card-header"
                        style={{
                          backgroundColor: "#041584",
                          padding: "0.75rem 1.25rem",
                          marginBottom: 0,
                        }}
                      >
                        <h2 className="mb-0">
                          <a
                            href="#"
                            type="button"
                            data-toggle="collapse"
                            data-target="#collapseThree"
                            aria-expanded="false"
                            aria-controls="collapseThree"
                            className="d-flex justify-content-between buttons collapsed"
                            style={{ textDecoration: "none" }}
                          >
                            <span
                              style={{
                                color: "#fff",
                                font: "500 20px/32px Roboto,Helvetica Neue,sans-serif",
                              }}
                            >
                              Let's Make Something Creative
                            </span>
                            {/* <FontAwesomeIcon icon="fa-solid fa-plus" /> */}
                            <p style={{ color: "white" }}>+</p>
                          </a>
                        </h2>
                      </div>
                    </div>
                    <div
                      id="collapseThree"
                      aria-labelledby="headingThree"
                      data-parent="#accordionExample"
                      className="collapse"
                    >
                      <div className="card-body">
                        {" "}
                        Anim pariatur cliche reprehenderit, enim eiusmod high
                        life accusamus terry richardson ad squid. 3 wolf moon
                        officia aute, non cupidatat skateboard dolor brunch.
                        Food truck quinoa nesciunt laborum eiusmod. 3 wolf moon
                        tempor, sunt aliqua put a bird on it squid single-origin
                        coffee nulla shoreditch et. Nihil anim keffiyeh
                        helvetica, craft beer labore wes anderson cred sapiente
                        ea proident. Ad vegan excepteur butcher vice lomo.
                        Leggings occaecat craft farm-to-table, raw denim
                        aesthetic synth nesciunt you probably haven't heard of
                        them labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                </ScrollAnimation>
              </div>
            </div>
          </ScrollAnimation>
        </div>
      </div>
    </div>
  );
}

export default ContainerFluidProjectSection;
