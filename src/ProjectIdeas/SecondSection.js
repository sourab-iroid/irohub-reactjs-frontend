import React from "react";
import MaskGroup15 from "../Asserts/Images/MaskGroup15.png";
import ScrollAnimation from "react-animate-on-scroll";
import mapLocationArrow from "../Asserts/Images/Icon map-location-arrow.svg";
import MaskGroup16 from "../Asserts/Images/MaskGroup16.png";
function SecondSection() {
  return (
    <div
      class="row"
      style={{
        display: "flex",
        flexWrap: "wrap",
        marginRight: "-15px",
        marginLeft: "-15px",
      }}
    >
      <div class="col-md-4 p-0">
        <ScrollAnimation animateIn="fadeInUp">
          <div
            class="img1 wow animate__ animate__fadeInUpBig animate__slow   animated"
            style={{ visibility: "visible" }}
          >
            <img
              src={MaskGroup15}
              class="img12"
              style={{
                zIndex: -6,
                position: "relative",
                width: "100%",
                height: "460px",
                top: "0px",
              }}
            />
          </div>
        </ScrollAnimation>
      </div>
      <div
        class="who-shape wow animate__ animate__zoomIn animate__slower"
        style={{ visibility: "visible" }}
      ></div>
      <div class="col-md-4">
        <div
          class="who wow animate__ animate__zoomIn animate__slow   animated"
          style={{
            visibility: "visible",
            animationName: "zoomInText",
            width: "inherit",
          }}
        >
          <img src={mapLocationArrow} className="mapLocationArrow" />
          <h1 class="who-head">WHO WE ARE</h1>
          <p class="who-para" style={{ fontSize: "14px" }}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. Lorem Ipsum is simply
            dummy text of the printing a nd typesetting industry. Lorem Ipsum
            has been the industry's standard dummy text ever since the 1500s,
            when an unknown printer took a galley of type and scrambled it to
            make a type specimen book. Lorem Ipsum is simply dummy text{" "}
          </p>
        </div>
      </div>
      <div class="col-md-4 p-0">
        <ScrollAnimation animateIn="fadeInUp">
          <img
            src={MaskGroup16}
            class="img2 wow animate__ animate__fadeInBottomRight animate__slow   animated"
            style={{
              visibility: "visible",
              animationName: "fadeInBottomRight",
            }}
          />
        </ScrollAnimation>
      </div>
    </div>
  );
}

export default SecondSection;
