import React from "react";

function Form() {
  return (
    <form>
      {/*  */}
      <div class="row" style={{ padding: "35px" }}>
        <div class="col">
          <label
            style={{ font: "bold 14px/20px Roboto,Helvetica Neue,sans-serif" }}
          >
            First name
          </label>
          <input
            type="text"
            id="border-bottom-input"
            class="form-control"
            // placeholder="First name"
          />
        </div>
        <div class="col">
          <label
            style={{ font: "bold 14px/20px Roboto,Helvetica Neue,sans-serif" }}
          >
            Last name
          </label>

          <input
            type="text"
            id="border-bottom-input"
            class="form-control"
            // placeholder="Last name"
          />
        </div>
        <label
          style={{ font: "bold 14px/20px Roboto,Helvetica Neue,sans-serif" }}
        >
          Email
        </label>

        <input
          type="text"
          id="border-bottom-input"
          class="form-control"
          // placeholder="Last name"
        />
        <label
          style={{ font: "bold 14px/20px Roboto,Helvetica Neue,sans-serif" }}
        >
          Contact Number
        </label>

        <input
          type="text"
          id="border-bottom-input"
          class="form-control"
          // placeholder="Last name"
        />
        <label
          style={{ font: "bold 14px/20px Roboto,Helvetica Neue,sans-serif" }}
        >
          Designation
        </label>

        <input
          type="text"
          id="border-bottom-input"
          class="form-control"
          // placeholder="Last name"
        />
        <div style={{ marginTop: "30px" }}>
          <label
            style={{ font: "bold 14px/20px Roboto,Helvetica Neue,sans-serif" }}
          >
            Upload your file
          </label>
          <div class="custom-file" style={{ marginTop: "15px" }}>
            <input
              type="file"
              className="custom-file-input"
              id="inputGroupFile01"
            />
          </div>
        </div>
        <div class="form-check form-check-inline" style={{ marginTop: "30px" }}>
          <input
            class="form-check-input"
            type="checkbox"
            id="inlineCheckbox1"
            value="option1"
          />
          <label class="form-check-label" for="inlineCheckbox1">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
          </label>
          <button
            type="button"
            class="btn btn-dark"
            style={{ marginTop: "30px" }}
          >
            Send message
          </button>
        </div>
      </div>
    </form>
  );
}

export default Form;
