import React from "react";
import img1 from "../Asserts/Images/img-01.png";
import ScrollAnimation from "react-animate-on-scroll";
export default function ThirdSection() {
  return (
    <div className="third">
      <div className="container">
        <div className="row">
          <div className="col-md-5">
            <ScrollAnimation animateIn="fadeInUp">
              <div
                className="mission wow animate__ animate__fadeInUpBig animate__slow   animated"
                style={{ visibility: "visible", animationName: "fadeInUpBig" }}
              >
                <p
                  className="mission-subhead"
                  style={{ color: "#ea4c89", fontSize: "18px" }}
                >
                  Our Mission -
                </p>
                <h1
                  className="mission-head"
                  style={{
                    fontSize: "36px",
                    fontWeight: 600,
                    lineHeight: "40px",
                  }}
                >
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit
                </h1>
                <p className="mission-para">
                  We provide digital experience services to startups and small
                  businesses to looking for a partner of their digital media,
                  design &amp; development, lead generation and communi cations
                  requirents. We work with you, not for you. Although we have a
                  great resources We provide digital experience services to
                  startups and small businesses to looking for a partner of
                  their digital media, design &amp; development, lead generation
                  and communi cations requirents. We work with you, not for you.
                  Although we have a great resources{" "}
                </p>
              </div>
            </ScrollAnimation>
          </div>
          <div className="col-md-7">
            <div className="d-flex">
              <ScrollAnimation animateIn="faseInUp">
                <div
                  className="shadow-sm p-3 mb-5 bg-white rounded mission-box wow animate__ animate__fadeInBottomLeft animate__slow   animated"
                  //   style="visibility: visible; animation-name: fadeInBottomLeft;"
                  style={{
                    visibility: "visible",
                    animationName: "fadeInBottomLeft",
                  }}
                >
                  <img src={img1} />
                  <h2 className="mission-card-head">Duis aute irure dolo</h2>
                  <p className="mission-card-para">
                    Duis aute irure dolor in repreh enderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pa riaturelit esse lie.
                  </p>
                </div>
              </ScrollAnimation>
              <ScrollAnimation animateIn="fadeInUp">
                <div
                  className="shadow-sm p-3 mb-5 bg-white rounded mission-box wow animate__ animate__fadeInBottomRight animate__slow   animated"
                  //   style="visibility: visible; animation-name: fadeInBottomRight;"
                  style={{
                    visibility: "visible",
                    animationName: "fadeInBottomRight",
                  }}
                >
                  <img src={img1} />
                  <h2 className="mission-card-head">Duis aute irure dolo</h2>
                  <p className="mission-card-para">
                    Duis aute irure dolor in repreh enderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pa riaturelit esse lie.
                  </p>
                </div>
              </ScrollAnimation>
            </div>
            <div className="d-flex">
              <ScrollAnimation animateIn="fadeInUp">
                <div
                  className="shadow-sm p-3 mb-5 bg-white rounded mission-box wow animate__ animate__fadeInBottomLeft animate__slow   animated"
                  style={{
                    visibility: "visible",
                    animationName: "fadeInBottomRight",
                  }}
                >
                  <img src={img1} />
                  <h2 className="mission-card-head">Duis aute irure dolo</h2>
                  <p className="mission-card-para">
                    Duis aute irure dolor in repreh enderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pa riaturelit esse lie.
                  </p>
                </div>
              </ScrollAnimation>

              <ScrollAnimation animateIn="fadeInUp">
                <div
                  className="shadow-sm p-3 mb-5 bg-white rounded mission-box wow animate__ animate__fadeInBottomRight animate__slow   animated"
                  style={{
                    visibility: "visible",
                    animationName: "fadeInBottomRight",
                  }}
                >
                  <img src={img1} />
                  <h2 className="mission-card-head">Duis aute irure dolo</h2>
                  <p className="mission-card-para">
                    Duis aute irure dolor in repreh enderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pa riaturelit esse lie.
                  </p>
                </div>
              </ScrollAnimation>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
