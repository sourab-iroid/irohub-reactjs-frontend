import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
import features from "../Asserts/Images/features-1.png";
function InvestmentSection() {
  return (
    // <ScrollAnimation animateIn="fadeIn">
    <div
      className="investment wow animate__ animate__fadeIn animate__slow   animated"
      // style="visibility: visible; animation-name: fadeIn;"
      style={{ visibility: "visible", animationName: "fadeIn" }}
    >
      <div class="container">
        <div class="row">
          <div class="col-md-7">
            <ScrollAnimation animateIn="fadeIn">
              <h1
                class="investment-head wow animate__ animate__fadeIn animate__slow"
                style="visibility: visible;"
                style={{ visibility: "visible" }}
              >
                Your Investment And Return.{" "}
              </h1>
            </ScrollAnimation>
            <ScrollAnimation animateIn="fadeIn">
              <p
                class="investment-para wow animate__ animate__fadeIn animate__slow"
                style={{ visibility: "visible" }}
              >
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut laboreet dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in repre henderit in voluptate velit cillum dolore eu fugiat
                nulla pariatur.Lorem ipsum dolor sit amet, consectetur
                adipisicing sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua. Ut enim ad minim veniam, nostrud exercitation
                ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                aute in repre henderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur.Lorem ipsum sit amet, consectetur
                adipisicing elit, sed do eiusmod tempor i ncididunt ut labore et
                aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                ullamco laboris nisi ut aliquip commodo consequat. Duis aute
                irure dolor in repre henderit in voluptate velit esse cillum eu
                fugiat nulla pariatur.
              </p>
            </ScrollAnimation>
          </div>

          <div class="col-md-5">
            <ScrollAnimation animateIn="bounceIn">
              <img
                src={features}
                class="img-fluid wow animate__ animate__bounceIn animate__slow"
                style={{ visibility: "visible" }}
              />
            </ScrollAnimation>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InvestmentSection;
