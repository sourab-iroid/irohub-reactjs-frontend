import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
import image from "../Asserts/Images/cropped-view-woman-filling-application-form.png";
import Form from "./Form";
function LastRow() {
  return (
    <div class="container-fluid last-row">
      <div class="row">
        <div class="col-md-6 p-0">
          {/* <ScrollAnimation animateIn="fadeIn"> */}
          <img
            src={image}
            class="img-fluid filling wow animate__ animate__fadeIn animate__slow   animated"
            // style="visibility: visible; animation-name: fadeIn;"
            style={{
              visibility: "visible",
              animationName: "fadeIn",
              position: "relative",
              top: "-280px",
              height: "135%",
              zIndex: -1,
            }}
          />
          {/* </ScrollAnimation> */}
        </div>
        <div class="col-md-6">
          <div class="last-sec">
            <h1 class="last-head">What you Need To Do ?</h1>
            <p class="last-para">
              Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
              nonumy{" "}
            </p>
            <Form />
          </div>
        </div>
      </div>
    </div>
  );
}

export default LastRow;
