import React from "react";
import ContainerFluidProjectSection from "./ContainerFluidProjectSection";
import FirstSection from "./FirstSection";
import FourthSection from "./FourthSection";
import InvestmentSection from "./InvestmentSection";
import SecondSection from "./SecondSection";
import ThirdSection from "./ThirdSection";
import LastRow from "./LastRow";
function ProjectIdeas() {
  return (
    <div className="Projectideas-style">
      <FirstSection />
      <SecondSection />
      <ThirdSection />
      <FourthSection />
      <ContainerFluidProjectSection />
      <InvestmentSection />
      <LastRow />
    </div>
  );
}

export default ProjectIdeas;
