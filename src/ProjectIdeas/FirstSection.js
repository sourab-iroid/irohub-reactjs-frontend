import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
import ss from "../Asserts/Images/ss.png";
import Rectangle from "../Asserts/Images/Rectangle.svg";
import Square from "../Asserts/Images/ss.svg";
import path1757 from "../Asserts/Images/Path 1757.svg";

function FirstSection() {
  return (
    <div className="container-fluid first">
      <div className="project-banner">
        <div className="right-cr-shape animate__animated animate__fadeInTopRight animate__slow"></div>
        <div className="bring-tri1 animate__animated animate__fadeInDownBig animate__slow"></div>
        <div className="bring-tri2 animate__animated animate__flip animate__slow"></div>
        <div className="row">
          <div className="col-md-5 p-0">
            <div id="diamond"></div>
            <ScrollAnimation animateIn="fadeInLeft">
              <div className="rectangle-shape1 animate__animated animate__fadeInLeftBig animate__slow">
                <img
                  src={Rectangle}
                  style={{
                    width: "150px",
                    position: "relative",
                    top: "40px",
                  }}
                  className="Rectangle"
                />

                {/* <img src={Rectangle} style={{ height: "50px" }} /> */}

                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="472"
                  height="70"
                  viewBox="0 0 472 70"
                ></svg>
              </div>
            </ScrollAnimation>

            <div className="rectangle-shape2 animate__animated animate__fadeInLeftBig animate__slow">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="472"
                height="70"
                viewBox="0 0 472 70"
              ></svg>
              <img
                src={Rectangle}
                style={{ height: "25px", position: "relative", top: "-70px" }}
                className="Rectangle"
              />
            </div>
            <div className="square-shape animate__animated animate__zoomIn animate__slow">
              {/* <svg
                xmlns="http://www.w3.org/2000/svg"
                width="374"
                height="331"
                viewBox="0 0 374 331"
              ></svg> */}
              <img
                src={Square}
                style={{
                  position: "relative",
                  left: "156px",
                  top: "-88px",
                  width: "270px",
                  animation: "zoomInText 2s ease-in",
                }}
              />
            </div>
            <div id="trapezium"></div>
            <img src={path1757} className="triangleUnderSquare" />

            {/* <ScrollAnimation animateIn="animate__jackInTheBox"> */}
            {/* <ScrollAnimation animateIn="zoomIn" afterAnimatedIn="rollOut"> */}
            <div
              className="banner-image animate__animated animate__jackInTheBox animate__slower"
              // style={{ animation: "zoomInText 3s ease-in" }}
            >
              <img
                src={ss}
                className="ban-img"
                style={{ width: "250px", position: "relative", left: "80px" }}
              />
            </div>
            {/* </ScrollAnimation> */}

            {/* </ScrollAnimation> */}

            <div className="banner-shape3 animate__animated animate__zoomIn animate__slow"></div>
          </div>
          <div className="col-md-7">
            <div className="banner-shape4"></div>
            <div
              className="bring-discription animate__animated animate__zoomIn animate__slow"
              style={{ marginTop: "60px", animation: "zoomInText 3s ease-in" }}
            >
              <h1
                className="banner-head"
                style={{
                  fontFamily: "Segoe UI",
                  fontWeight: "bold",
                  paddingLeft: "100px",
                }}
              >
                {" "}
                Bring All Your Work Together{" "}
              </h1>
              <p
                className="banner-para"
                style={{
                  fontSize: "x-small",
                  paddingLeft: "100px",
                }}
              >
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. Lorem
                Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown{" "}
              </p>
              <button
                type="button"
                class="btn btn-outline-secondary"
                style={{
                  position: "relative",
                  left: "100px",
                }}
              >
                Learn More
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  class="bi bi-arrow-down"
                  viewBox="0 0 16 16"
                  style={{ position: "relative", left: "5px" }}
                >
                  <path
                    fill-rule="evenodd"
                    d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z"
                  />
                </svg>
              </button>
            </div>
            <img src={path1757} className="triangleOnRightSide" />
          </div>
        </div>
        <div className="bring-tri3 animate__animated animate__slideInUp animate__slow"></div>
        <div className="bring-tri4 animate__animated animate__rollIn animate__slow"></div>
      </div>
    </div>
  );
}

export default FirstSection;
