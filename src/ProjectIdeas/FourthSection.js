import React from "react";

import bulbImage from "../Asserts/Images/Untitled-1-01.png";
import personImage from "../Asserts/Images/home_01-1.png";
import ScrollAnimation from "react-animate-on-scroll";
function FourthSection() {
  return (
    <div>
      <div class="container-fluid fourth">
        <div class="row mb-5">
          <div class="col-md-6">
            <div class="img-section">
              <div
                class="circle1 wow animate__ animate__fadeInTopLeft animate__slow   animated"
                style={{
                  visibility: "visible",
                  animationName: "fadeInTopLeft",
                }}
              ></div>
              <div
                class="circle2 wow animate__ animate__fadeInBottomRight animate__slow   animated"
                // style="visibility: visible;"
                style={{ visibility: "visible" }}
              ></div>
              <ScrollAnimation animateIn="slideInLeft">
                <img
                  src={bulbImage}
                  class="bulb-img wow animate__ animate__slideInLeft animate__slow   animated"
                  style={{ visibility: "visible" }}
                />
              </ScrollAnimation>

              <img
                src={personImage}
                class="img123 wow animate__ animate__zoomIn animate__slower   animated"
                style={{
                  visibility: "visible",
                  animation: "zoomInText 3s ease-in",
                }}
              />
              <div
                class="circle3 wow animate__ animate__fadeInUpBig animate__slow   animated"
                style={{ visibility: "visible", animationName: "fadeInUpBig" }}
              ></div>
            </div>
          </div>
          <div class="col-md-6">
            <h1
              class="method-head wow animate__ animate__zoomIn animate__slow   animated"
              style={{
                visibility: "visible",
                animation: "zoomInText 3s ease-in",
              }}
            >
              Our Method
            </h1>
            <p
              class="method-para wow animate__ animate__zoomIn animate__slow   animated"
              style={{
                visibility: "visible",
                animation: "zoomInText 3s ease-in",
              }}
            >
              We provide digital experience services to startups and small
              businesses to looking for a partner of their digital media, design
              &amp; development, lead generation and communi cations requirents.
              We work with you, not Although we have a great resources We
              provide digital experience services to startups and small to
              looking for a partner of their digital media, design &amp;
              development, lead generation and communi requirents. We work with
              you, not for you. Although we have a great resources We provide
              digital services to startups and small businesses to looking for a
              partner of their digital media, design &amp; lead generation and
              communi cations requirents. We work with you, not for you.
              Although we have a We provide digital experience services to
              startups and small businesses to looking for a partner of digital
              media, design &amp; development, lead generation and communi
              cations requirents. We work with for you. Although we have a great
              resources
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FourthSection;
