import React from "react";
import IrohubLogo from "../Asserts/logo/IrohubLogo.png";
import { DialogSignIn } from "../Component/DialogSignIn";
import { GetInTouchDialog } from "../Component/GetInTouchDialog";
import { AiOutlineMenu } from "react-icons/ai";
import { DialogSignUp } from "../Component/DialogSignUp";
export const Header = () => {
  // function getHandle
  //     let getin_touch = document.getElementById("getin_touch");
  //     getin_touch.style.border = "none";
  const [getInTouch, setGetInTouch] = React.useState(false);
  const [showSignIn, setShowSignin] = React.useState(false);
  const [showSignUp, setShowSignUp] = React.useState(false);
  let arrayOfCourses = [
    "Python Development",
    "Java Development",
    "PHP Development",
    "Android Development",
    "IOS Development",
    "Web Development",
  ];

  // ;
  return (
    <>
      <header className="header-style ">
        <div className="container ">
          <nav className="row navbar navbar-expand-lg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  ">
            <div className="container-fluid">
              <a className="navbar-brand" href="/">
                <img
                  src={IrohubLogo}
                  alt="Irohublogo"
                  className="image-fluid brand-logo"
                />
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon">
                  <AiOutlineMenu />
                </span>
              </button>
              <div
                className="collapse navbar-collapse head-nav"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav  ">
                  <li className="dropdown nav-item">
                    <a
                      className="nav-link active navigations"
                      aria-current="page"
                      href="/liveClasses"
                    >
                      {/* Live Classes */}
                      <button class="dropbtn nav-link p-0">Live Classes</button>
                    </a>
                    <div class="dropdown-content">
                      {arrayOfCourses.map((i) => {
                        return (
                          <div
                            onClick={() =>
                              localStorage.setItem("courseSectionClicked", i)
                            }
                          >
                            <a href="/artBoard">{i}</a>
                          </div>
                        );
                      })}
                    </div>
                  </li>
                  <li className="dropdown  nav-item">
                    <a className="nav-link navigations" href="/videoClasses">
                      {/* Video Classes */}
                      <button class="dropbtn nav-link p-0">
                        Video Classes
                      </button>
                    </a>
                    <div class="dropdown-content">
                      {arrayOfCourses.map((i) => {
                        return (
                          <div
                            onClick={() =>
                              localStorage.setItem("courseSectionClicked", i)
                            }
                          >
                            <a href="/artBoard">{i}</a>
                          </div>
                        );
                      })}
                    </div>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link navigations" href="/placement">
                      Testimonials
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link navigations" href="/services">
                      Our Services
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link navigations" href="#">
                      <a
                        // className="btn dialog-btn p-0 navigations "
                        data-toggle="modal"
                        data-target="#contactModal"
                        onClick={() => {
                          setGetInTouch(true);
                        }}
                      >
                        Get In Touch
                      </a>
                      {getInTouch && <GetInTouchDialog />}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link navigations" href="/scholarship">
                      Scholarship
                    </a>
                  </li>
                  {/* <li>
                    <div className="text-right">
                      <button className="btn btn-outline-primary button-sign ms-4">
                        Sign In
                      </button>
                      <button className="btn btn-outline-primary button-sign ms-3">
                        Sign Up
                      </button>
                    </div>
                  </li> */}
                </ul>
              </div>
              <form className="d-flex head-form">
                {/* <button
                  className="btn btn-outline-primary"
                  type="submit"
                  data-toggle="modal"
                  data-target="#exampleModal"
                  // onClick={() => {
                  //   setShowSignin(true);
                  // }}
                >
                  Sign In
                </button> */}
                <button
                  type="button"
                  class="btn btn-outline-primary"
                  data-toggle="modal"
                  data-target="#exampleModal"
                  onClick={() => setShowSignin(true)}
                >
                  Sign In
                </button>
                {showSignIn && <DialogSignIn />}

                {/* {showSignIn && <DialogSignIn />} */}

                <button
                  type="button"
                  class="btn btn-outline-primary"
                  data-toggle="modal"
                  data-target="#signUpModal"
                  style={{ position: "relative", left: "10px" }}
                  onClick={() => setShowSignUp(true)}
                >
                  Sign up
                </button>
                {showSignUp && (
                  <DialogSignUp
                    showSignUp={showSignUp}
                    setShowSignUp={setShowSignUp}
                  />
                )}
              </form>
            </div>
          </nav>
        </div>
      </header>
    </>
  );
};
