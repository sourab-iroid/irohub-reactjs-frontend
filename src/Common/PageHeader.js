import React from "react";
import lapTyping from "../Asserts/Images/lapTyping.png";
export const PageHeader = (props) => {
  let { pageName } = props;
  return (
    <>
      <div className="black-bg animate__animated animate__fadeInLeftBig">
        <div className="left-clip animate__animated animate__fadeInLeftBig animate__slow"></div>
        <div className="title text-left animate__animated animate__zoomIn animate__slow">
          <h1 className="title-main">{pageName}</h1>
          <h3 className="title-sub">LOREM IPSUM</h3>
        </div>
      </div>
      <div className="text-right middle-clip animate__animated animate__fadeInUp animate__delay-1s animate__slow">
        <img
          //   src="../../../assets/image/placement-middle.png"
          src={lapTyping}
          alt=""
          className="middle-img"
        />
        <div className="middle-img-shape"></div>
        {/* </div> */}

        <div className="right-clip animate__animated animate__fadeInRightBig animate__slow">
          <div className="right-clip-inside"></div>
        </div>
      </div>
    </>
  );
};
