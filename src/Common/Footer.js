import React from "react";
import IrohubLogo from "../Asserts/logo/IrohubLogo.png";
import { AiOutlineRight } from "react-icons/ai";
import { FaAngleRight } from "react-icons/fa";
import { FaChevronRight } from "react-icons/fa";

export const Footer = () => {
  return (
    <>
      <footer
        className="footer-style"
        // data-aos="fade-up fade-slow"
        // data-aos-duration="3000"
      >
        <div className="shapes-footer">
          <div className="black-shape"></div>
          <div className="blue-right"></div>
          <div className="blue-left"></div>
        </div>
        <div className="black-bg">
          <div className="container foot-con">
            <div className=" row wow">
              <div className="col-md-4">
                <img src={IrohubLogo} className="foot-logo" />
                <p className="footer-para">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Laborum obcaecati dignissimos quae quo ad iste ipsum officiis
                  deleniti asperioressit.
                </p>
              </div>
              <div className="col-md-5 ">
                <h3 className="small-headings mt-3">LINKS</h3>
                <hr className="hr-small" />
                <div className="row">
                  <div className="col-sm-5 col-md-5 text-white ">
                    <ul id="list">
                      <li>
                        <span className="icon-style">
                          <FaChevronRight />
                        </span>
                        <a
                          href="/aboutUs"
                          className="ms-3"
                          style={{ textDecoration: "none", color: "white" }}
                        >
                          About Us
                        </a>
                      </li>
                      <li>
                        <span className="icon-style">
                          <FaChevronRight />
                        </span>
                        <a
                          href="/blog"
                          className="ms-3 "
                          style={{ textDecoration: "none", color: "white" }}
                        >
                          Blog
                        </a>
                      </li>
                      <li>
                        <span className="icon-style">
                          <FaChevronRight />
                        </span>
                        <a
                          href="/faq"
                          className="ms-3 "
                          style={{ textDecoration: "none", color: "white" }}
                        >
                          Faq
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="col-sm-7 col-md-7 p-0 text-white">
                    <ul id="list">
                      <li>
                        <span className="icon-style">
                          <FaChevronRight />
                        </span>
                        <a
                          href="/contact_us"
                          className="ms-3 "
                          style={{ textDecoration: "none", color: "white" }}
                        >
                          Contact Us
                        </a>
                      </li>
                      <li>
                        <span className="icon-style">
                          <FaChevronRight />
                        </span>
                        <a
                          href="/termsAndConditions"
                          className="ms-3 "
                          style={{ textDecoration: "none", color: "white" }}
                        >
                          Terms & Contitions
                        </a>
                      </li>
                      <li>
                        <span className="icon-style">
                          <FaChevronRight />
                        </span>
                        <a
                          href="/privacy_policy"
                          className="ms-3 "
                          style={{ textDecoration: "none", color: "white" }}
                        >
                          Privacy Policy
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-md-3 text-white">
                <h3 className="small-headings mt-3">SERVICES</h3>
                <hr className="hr-small " />
                <ul id="list">
                  <li>
                    <span className="icon-style">
                      <FaChevronRight />
                    </span>
                    <a
                      href="/projectIdeas"
                      className="ms-3 "
                      style={{ textDecoration: "none", color: "white" }}
                    >
                      Project Ideas
                    </a>
                  </li>
                  <li>
                    <span className="icon-style">
                      <FaChevronRight />
                    </span>
                    <a
                      href="/liveClasses"
                      className="ms-3 "
                      style={{ textDecoration: "none", color: "white" }}
                    >
                      Live Training
                    </a>
                  </li>
                  <li>
                    <span className="icon-style">
                      <FaChevronRight />
                    </span>
                    <a
                      href="/videoClasses"
                      className="ms-3 "
                      style={{ textDecoration: "none", color: "white" }}
                    >
                      Video Training
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <hr className="mt-4 mb-4" />
            <div className="row pb-5 ">
              <div className="col-md-4">
                <h3 className="small-headings mt-3">OFFICE ADDRESS</h3>
                <hr className="hr-small" />
                <p className="footer-para">
                  1st Floor, Trust building, Kayyath Ln, <br /> Palarivattom,
                  Kochi{" "}
                </p>
              </div>
              <div className="col-md-5">
                <h3 className="small-headings mt-3">QUICK CONTACTS</h3>
                <hr className="hr-small" />
                <p className="footer-para">
                  info@Irohub.com <br /> +91 8129855155{" "}
                </p>
              </div>
              <div className="col-md-3">
                <h3 className="small-headings mt-3">CONNECTED</h3>
                <hr className="hr-small" />

                <ul className="Socialicon list-unstyled">
                  <li className="">
                    <img
                      src="https://www.cookiemanindia.com/assets/images/svg/twitter_icon.svg"
                      class="img-fluid "
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://www.cookiemanindia.com/assets/images/svg/facebook_icon.svg"
                      class="img-fluid ms-1"
                      alt=""
                    />
                  </li>
                  <li>
                    <img
                      src="https://www.cookiemanindia.com/assets/images/svg/instagram_icon.svg"
                      class="img-fluid ms-2"
                      alt=""
                    />
                  </li>
                  {/* <li>
                    <img
                      src="https://www.cookiemanindia.com/assets/images/svg/youtube_icon.svg"
                      class="img-fluid "
                      alt=""
                    />
                  </li> */}
                  <li>
                    <img
                      src="https://www.cookiemanindia.com/assets/images/svg/linkedin_icon.svg"
                      class="img-fluid  ms-2"
                      alt=""
                    />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="blue-bottom">
            <p className="rights-bottom ">
              © IROHUB. ALL Rights Reserved. Powered by Iroid Technologies
            </p>
          </div>
        </div>
      </footer>
    </>
  );
};
